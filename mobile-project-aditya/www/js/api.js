api=new Object();
api.root=appSettings.APIUrl;


api.Url=api.root;

api.post = function(actionUrl,parms,callBack){
    $.post(api.Url+actionUrl,parms,function(data){
    callBack(data);
    }).fail(function(error){
    Errorlog(api.Url+actionUrl,error,parms);
    });
    };


api.post1 = function(actionUrl,params,callBack){
    // $.ajaxSetup({
    // 'beforeSend' : function(xhr) {
    // xhr.overrideMimeType('application/json; charset=utf-8');
    // },
    // });
    // $.post(api.Url+actionUrl,JSON.stringify(params),function(data){
    // callBack(data);
    // } ).fail(function(error){
    // Errorlog(api.Url+actionUrl,error,parms);
    // });

    $.ajax({
    type: "POST",
    contentType: "application/json",
    url: api.Url+actionUrl,
    data: JSON.stringify(params),
    dataType: "json",
    success: callBack,
    // error: function(error){
    // Errorlog(api.Url+actionUrl,error,parms);
    // }
    });
    };


api.get = function(actionUrl,callBack){

    $.get(api.Url+actionUrl, function(data){
        callBack(data);
    }).fail(function(error){
        Errorlog(api.Url+actionUrl,error);
    });
};

function Errorlog(url,error,postdata){
    console.log(url);
    console.log(postdata);
    console.log(error);
    console.log(error.responseText);
}

// api call without authentication start
api.loginValidation=function(sessId,email,password,callBack)
{
    var postData = '{"sessid":"'+sessId+'","username":"'+email+'","password":"'+password+'"}';
    api.post('loginpeople',postData,function(data){
        callBack(data);
    });
};

// api call to get session
api.getSession=function(callBack)
{
    api.get('getsessionid',function(data){
        callBack(data);
    });
};

// api call to check the whether user loggedin or not
api.isLoggedIn=function(sessId,userId,callBack)
{
    var postData = '{"sessid":"'+sessId+'","userid":"'+userId+'"}';
    api.post('uservalidsession',postData,function(data){
        callBack(data);
    });
};

// api call to logout the user
api.logout=function(sessId,callBack)
{
    var postData = '{"sessid":"'+sessId+'"}';
    api.post('userlogout',postData,function(data){
        callBack(data);
    });
};

// api call to add the vendor
api.addVendor=function(name,address1,address2,email,phone,companyid,bankname,ac,holdername,ifsc,payment,aadhaar,pan,gstform,regform,comments,isvarified,status,userid,comlocation,callBack)
{
    var postData = '{"vendor_name":"'+name+'","address_1":"'+address1+'","address_2":"'+address2+'","email":"'+email+'","phone_number":"'+phone+'","company_id":"'+companyid+'","bank_name":"'+bankname+'","bank_accountno":"'+ac+'","ifsc_code":"'+ifsc+'","holder_name":"'+holdername+'","max_amount":"'+'1000'+'","mode":"'+payment+'","doc_aadhar":"'+aadhaar+'","doc_pan":"'+pan+'","doc_gst":"'+gstform+'","doc_reg_form":"'+regform+'","doc_comments":"'+comments+'","doc_is_verified":"'+isvarified+'","status":"'+status+'","added_by":"'+userid+'","location_name":"'+comlocation+'"}';
    api.post('addvendor',postData,function(data){
        callBack(data);
    });
};

// api call to add the item
api.addItem=function(itemname,itemshortname,itemcategory,itemcost,itemdefalutprice,qty,measureunit,status,companyid,puserid,callBack)
{
    var postData = '{"item_name":"'+itemname+'","item_short_name":"'+itemshortname+'","category_id":"'+itemcategory+'","item_cost":"'+itemcost+'","item_default_price":"'+itemdefalutprice+'","quantity":"'+qty+'","size":"'+measureunit+'","status":"'+status+'","company_id":"'+companyid+'","added_by":"'+puserid+'"}';
    api.post('additems',postData,function(data){
        callBack(data);
    });
};

// api call to get brands
api.getBrands=function(callBack)
{
    api.get('getcompanies',function(data){
        callBack(data);
    });
};

// api call to get measureofunit
api.getUnits=function(callBack)
{
    api.get('getunits',function(data){
        callBack(data);
    });
};
//category
api.getcategory=function(callBack)
{
    api.get('getcategories',function(data){
        callBack(data);
    });
};
// api call to get vendors/suppliers
api.getVendors=function(callBack)
{
    api.get('getvendors',function(data){
        callBack(data);
    });
};

// api call to remove the vendor details
api.removeVendor=function()
{
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
       return;
    });
};

// api call to view the vendor details
api.viewVendor=function(supId,userId,callBack)
{
    var postData = '{"supplier_id":"'+supId+'","user_id":"'+userId+'"}';
    api.post('viewvendor',postData,function(data){
        callBack(data);
    });
};

// api call to edit the vendor details
api.editVendor=function(supId,name,address1,address2,email,phone,companyid,bankname,ac,holdername,ifsc,payment,aadhaar,pan,gstform,regform,comments,isvarified,status,userid,comlocation,callBack)
{
    var postData = '{"supplier_id":"'+supId+'","vendor_name":"'+name+'","address_1":"'+address1+'","address_2":"'+address2+'","email":"'+email+'","phone_number":"'+phone+'","company_id":"'+companyid+'","bank_name":"'+bankname+'","bank_accountno":"'+ac+'","ifsc_code":"'+ifsc+'","holder_name":"'+holdername+'","max_amount":"'+'1000'+'","mode":"'+payment+'","doc_aadhar":"'+aadhaar+'","doc_pan":"'+pan+'","doc_gst":"'+gstform+'","doc_reg_form":"'+regform+'","doc_comments":"'+comments+'","doc_is_verified":"'+isvarified+'","status":"'+status+'","added_by":"'+userid+'","location_name":"'+comlocation+'"}';
    api.post('editvendor',postData,function(data){
        callBack(data);
    });
};
// api call to edit the product details
api.editProduct=function(supId,userId,callBack)
{
    var postData = '{"item_id":"'+supId+'","user_id":"'+userId+'"}';
    api.post('viewitem',postData,function(data){
        callBack(data);
    });
};

//get po items by po id
api.viewpoitems=function(supId,userId,callBack)
{
    var postData = '{"POs_id":"'+supId+'","user_id":"'+userId+'"}';
    api.post('poitemsbypoid',postData,function(data){
        callBack(data);
    });
}

//get po products by grn id
api.viewProdsByGrn=function(supId,userId,callBack)
{
    var postData = '{"grn_id":"'+supId+'","user_id":"'+userId+'"}';
    api.post('getitemsbygrnid',postData,function(data){
        callBack(data);
    });
}