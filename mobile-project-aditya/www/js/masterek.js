var body = $('body');
var itemjsondata = '';
var itemdetailsarray = [];
var alltaxarrays = [];
var qtyarray = [];
var totalarray = [];
var measurementarray = [];
var pricearray = [];
var runningstockarray = [];
var gstarray = [];
var cgstarray = [];
var sgstarray = [];
var itemtotalpricearray = [];
var itemidarray = [];
var gsttypeidarray = [];
var cgsttypeidarray = [];
var sgsttypeidarray = [];
var gstcalarray = [];
var cgstcalarray = [];
var sgstcalarray = [];
var totaltaxamt = [];
var itemnames = [];
var quotations = [];
var checkitemname = [];
var data123 = {};
var totalitemqty = "";
var selectedVendor = '';
var overlay = new Object();
var purvendorid = null;
var purcompany = "";
overlay.show = function (addClass) {
	$('.overlay').addClass('on').show();
	if (addClass != undefined) {
		$('.overlay').addClass(addClass);
	}
};
overlay.hide = function () {
	$('.overlay').removeClass('on').hide();
	$('.overlay-hide').hide('fast');
	$('.overlay-remove').remove();
};
body.on('click', '.accountpop', function (e) {
	e.stopPropagation();
});
body.on('click', '.act-my-account', function (e) {
	e.stopPropagation();
	$('.accountpop').css({
		left: 0
	});
});
body.on('click', '.side-barmenu', function (e) {
	e.stopPropagation();
	$('.accountpop').css({
		left: '-275px'
	});
});
$(document).on("click", function () {
	$(".accountpop").css({
		left: '-275px'
	});
});
//DROPDOWN Menu
body.on('click', '.side-menubar', function () {
	const t = $(this);
	t.toggleClass('open');
	t.next().toggle('slow');
});
//menu routing code here
body.on('click', '.act-menu', function () {
	var t = $(this);
	var extra = t.data('extra');
	if (extra == '') {
		extra = 'rescan';
	}
	pageName = t.attr('href');
	pageName = pageName.substr(1);
	page.route(pageName, $(this), extra);
	//change password page//
	if (pageName == "changepassword") {
		$(".login").addClass("hide");
		$(".vendor-details").addClass("hide");
		$(".profile-update").addClass("hide");
		$(".purchase-payments").addClass("hide");
		$(".payments-list").addClass("hide");
		$(".change-password").removeClass("hide");
		$(".purchaselist").addClass("hide");
		$(".cash-book").addClass("hide");
	}
	//profile update page//
	if (pageName == "profileupdate") {
		$(".login").addClass("hide");
		$(".vendor-details").addClass("hide");
		$(".change-password").addClass("hide");
		$(".purchase-payments").addClass("hide");
		$(".payments-list").addClass("hide");
		$(".profile-update").removeClass("hide");
		$(".purchaselist").addClass("hide");
		$(".cash-book").addClass("hide");
		appdata.profileupdate();
	}
	//purchase list page
	if (pageName == "Purchases") {
		$(".login").addClass("hide");
		$(".vendor-details").addClass("hide");
		$(".profile-update").addClass("hide");
		$(".purchase-payments").addClass("hide");
		$(".change-password").addClass("hide");
		$(".purchaselist").removeClass("hide");
		$(".payments-list").addClass("hide");
		$(".purchase-payments").addClass("hide");
		$(".itemdetailsview").addClass("hide");
		$(".cash-book").addClass("hide");
		var d = new Date();
		var month = d.getMonth() + 1;
		var lastmonth = d.getMonth();
		var day = d.getDate();
		var today = d.getFullYear() + '-' +
			(('' + month).length < 2 ? '0' : '') + month + '-' +
			(('' + day).length < 2 ? '0' : '') + day;

		var lastmonthdate = d.getFullYear() + '-' +
			(('' + lastmonth).length < 2 ? '0' : '') + lastmonth + '-' +
			(('' + day).length < 2 ? '0' : '') + day;
		$('#dtpurchasetodate').val(today);
		$('#dtpurchasefromdate').val(lastmonthdate);
		appdata.getpurvendor(); //get vendor details
		appdata.getcompanys();
		appdata.getpurchaselist(today, lastmonthdate, purcompany, purvendorid);
	}

	// Cash book Page 
	if (pageName == "cashbook") {
		$(".login").addClass("hide");
		$(".vendor-details").addClass("hide");
		$(".change-password").addClass("hide");
		$(".purchase-payments").addClass("hide");
		$(".payments-list").addClass("hide");
		$(".profile-update").addClass("hide");
		$(".purchaselist").addClass("hide");
		$(".cash-book").removeClass("hide");
		$(".bank-book").addClass("hide");
		$(".total-balance").addClass("hide");

		$(".cashbook-list-attach").addClass("hide");
		appdata.cashbookavailablebal(); // get available cash for company 
		appdata.getaccountheadnames(); // get all list of account heads
		$("#fromdate").datepicker().datepicker('setDate', new Date().getMonth() +
			'/' + new Date().getDate() + '/' + new Date().getFullYear());
	}

	//Bank Book
	if (pageName == "bankbook") {
		$(".login").addClass("hide");
		$(".vendor-details").addClass("hide");
		$(".change-password").addClass("hide");
		$(".purchase-payments").addClass("hide");
		$(".payments-list").addClass("hide");
		$(".profile-update").addClass("hide");
		$(".vendor-details").addClass("hide");
		$(".purchaselist").addClass("hide");
		$(".cash-book").addClass("hide");
		$(".total-balance").addClass("hide");
		$(".bank-book").removeClass("hide");
		appdata.getallbankaccounts();


	}



});
//login form code here
body.on('click', '.loginfom', function () {
	$('.loading').addClass('spinner');
	var email = $("#email").val();
	var password = $("#password").val();
	if (email == "") {
		var mes = "Please enter user name";
		appdata.warningmsg(mes);
		$('.loading').removeClass('spinner');
		return;
	}
	if (password == "") {
		var mes = "Please enter password";
		appdata.warningmsg(mes);
		$('.loading').removeClass('spinner');
		return;
	}
	$('#vendor').val(0);
	appdata.loginform(email, password); //login method
	appdata.getvendor(); //get vendor details
	appdata.getuserscompanylist(email);
});
//insert item details code here
$('.gotopurchaselist').click(function () {
	$('.loading').addClass('spinner');
	itemdetailsarray = [];
	alltaxarrays = [];
	qtyarray = [];
	totalarray = [];
	measurementarray = [];
	pricearray = [];
	runningstockarray = [];
	gstarray = [];
	cgstarray = [];
	sgstarray = [];
	itemtotalpricearray = [];
	itemidarray = [];
	gsttypeidarray = [];
	cgsttypeidarray = [];
	sgsttypeidarray = [];
	gstcalarray = [];
	cgstcalarray = [];
	sgstcalarray = [];
	totaltaxamt = [];
	quotations = [];
	$('.gstdata').each(function () {
		gstarray.push($(this).attr('data-tes'));
		console.log($(this).attr('data-tes'));
	});
	$('.cgstdata').each(function () {
		cgstarray.push($(this).attr('data-tes'));
		console.log($(this).attr('data-tes'));
	});
	$('.sgstdata').each(function () {
		sgstarray.push($(this).attr('data-tes'));
		console.log($(this).attr('data-tes'));
	});
	$('.datameasure').each(function () {
		measurementarray.push($(this).attr('data-tes'));
		console.log($(this).attr('data-tes'));
	});
	$('.itemprice').each(function () {
		pricearray.push($(this).attr('data-tes'));
	});
	$('.itemstock').each(function () {
		runningstockarray.push($(this).attr('data-tes'));
	});

	$("input[type='number'].qty").each(function () {
		qtyarray.push($(this).val());
	});
	$("input[type='text'].searchitem").each(function () {
		itemnames.push($(this).val());
		console.log(itemnames);
	});
	$('.itemtotalprice').each(function (event, ui) {
		var data = ui.innerHTML;
		itemtotalpricearray.push(data);
	});
	$('.itemid').each(function () {
		itemidarray.push($(this).attr('data-tes'));
	});

	$('.gsttypeid').each(function () {
		gsttypeidarray.push($(this).attr('data-tes'));
	});
	$('.cgsttypeid').each(function () {
		cgsttypeidarray.push($(this).attr('data-tes'));
	});
	$('.sgsttypeid').each(function () {
		sgsttypeidarray.push($(this).attr('data-tes'));
	});
	$('.totaltaxamt').each(function () {
		totaltaxamt.push($(this).attr('data-tes'));
	});
	//all tax cal amt
	$('.gstcal').each(function () {
		gstcalarray.push($(this).attr('data-tes'));
	});
	$('.sgstcal').each(function () {
		sgstcalarray.push($(this).attr('data-tes'));
	});
	$('.cgstcal').each(function () {
		cgstcalarray.push($(this).attr('data-tes'));
	});
	var leng = itemidarray.length;



	for (var i = 0; i < leng; i++) {
		alltaxarrays.push({
			"tax_type_id": 1,
			"percentage": 12,
			"tax_amount": 10,
		});
	}
	var alltaxarrays = [];
	for (var i = 0; i < leng; i++) {
		alltaxarrays.push({
			"tax_type_id": gsttypeidarray[i],
			"percentage": gstarray[i],
			"tax_amount": gstcalarray[i],
		});
		alltaxarrays.push({
			"tax_type_id": sgsttypeidarray[i],
			"percentage": sgstarray[i],
			"tax_amount": sgstcalarray[i],
		});
		alltaxarrays.push({
			"tax_type_id": cgsttypeidarray[i],
			"percentage": cgstarray[i],
			"tax_amount": cgstcalarray[i],
		});
		itemdetailsarray.push({
			"item_id": itemidarray[i],
			"quantity": qtyarray[i],
			"price": pricearray[i],
			"total_amount": itemtotalpricearray[i],
			"discount": 0,
			"discount_amount": 0,
			"item_taxes": alltaxarrays
		});
	}
	var quantitytotal = 0;
	for (var i = 0; i < leng; i++) {
		var qtye = parseInt(qtyarray[i]);
		quantitytotal += qtye;
	}
	var totaliteamt = 0;
	for (var i = 0; i < leng; i++) {
		var totite = parseInt(itemtotalpricearray[i]);
		totaliteamt += totite;
	}
	var totaltaxamt1 = 0;
	for (var i = 0; i < leng; i++) {
		var totite = parseInt(totaltaxamt[i]) * quantitytotal;
		totaltaxamt1 += totite;
	}
	var eachitemprice = 0;
	for (var i = 0; i < leng; i++) {
		var totite = parseInt(pricearray[i]) * quantitytotal;
		eachitemprice += totite;
	}
	var vendorid = selectedVendor;
	appdata.insertitemdtls(itemdetailsarray, alltaxarrays, quantitytotal, vendorid, totaliteamt, totaltaxamt1, eachitemprice);
});
//search purchase list from date to todate
$('.searchdatafrmdtetodte').click(function () {
	var currentmonthdate = $('#dtpurchasetodate').val();
	var lastmonthdate = $('#dtpurchasefromdate').val();
	var company_id =
		appdata.getpurvendor(); //get vendor details
	appdata.getcompanys();
	if (purcompany != "") {
		purcompany = purcompany;
	}
	if (purvendorid != "") {
		purvendorid = purvendorid;
	}
	appdata.getpurchaselist(currentmonthdate, lastmonthdate, purcompany, purvendorid);
});
//get vendor id
$('#vendor').change(function () {
	$('.loading').addClass('spinner');
	selectedVendor = parseInt(jQuery(this).val());
	appdata.getvendordetails(selectedVendor);
});
//get item id
$('#item').change(function () {
	var selectedValue = parseInt(jQuery(this).val());
	appdata.getitemdetails(selectedValue);
});
//get purchase company id
$('#purcompany').change(function () {
	purcompany = parseInt(jQuery(this).val());

});


$('.itemtobck').click(function () {
	$('.showvendordtls').removeClass('hide');
	$('.itemdetailsview').addClass('hide');

});

var itemCount = 0;
$('.add').click(function () {
	itemCount++;
	$('#itemCount').html(itemCount).css('display', 'block');
});
$('.clear').click(function () {
	itemCount = 0;
	$('#itemCount').html('').css('display', 'none');
	$('#cartItems').html('');
});

//INCREASE COUNT QUANTITY COUNT
function increaseValue(id, index, availableQuantity) {
	//$('.grandtotal' + id + '').empty();
	var value = parseInt(document.getElementById('number-' + id + '').value, 10);
	value = isNaN(value) ? 0 : value;
	value++;
	if (value > availableQuantity) {
		mes = "More than running stock not allowed";
		appdata.warningmsg(mes);
		return;
	}
	document.getElementById('number-' + id + '').value = value;
	// var data = $(".price-" + id).val();
	// // var gst = $(".gst-" + id + "").text();
	// var cgst = $(".cgst-" + id + "").text();
	// var sgst = $(".sgst-" + id + "").text();
	// // var gstspl = gst.split("%");
	// var sgstspl = sgst.split("%");
	// var cgstspl = cgst.split("%");
	// var total = data * value;
	// // var gstcal = total * gstspl[0] / 100;
	// var sgstcal = total * sgstspl[0] / 100;
	// var cgstcal = total * cgstspl[0] / 100;
	// var tot = total +  sgstcal + cgstcal;
	// $('.grandtotal' + id + '').html(tot);

	var SGST = $(".sgst-" + id + "").text();
	var CGST = $(".cgst-" + id + "").text();
	var GST = $(".gst-" + id + "").text();

	var gstspl = GST.split("%");
	var sgstspl = SGST.split("%");
	var cgstspl = CGST.split("%");

	var gsttypeid = 0;
	var sgsttypeid = 0;
	var cgsttypeid = 0;
	var discount;
	if (index == '' || index == undefined) {
		discount = $('#disc').val();
	}
	else {
		discount = $('#disc' + index).val();
	}
	if (discount == '' || discount == undefined) {
		var disc = 0;
	} else {
		var disc = discount;
	}

	var total = $('#itemrate' + id).val() * value;
	var discAmt = (total * disc) / 100;
	var totdisAmt = total - discAmt;
	var gstcal = totdisAmt * gstspl[0] / 100;
	var sgstcal = totdisAmt * sgstspl[0] / 100;
	var cgstcal = totdisAmt * cgstspl[0] / 100;
	var totaltaxamt;
	if (gstspl[0] != 0 || gstspl[0] != 0.00) {
		totaltaxamt = gstcal;
	} else {
		totaltaxamt = sgstcal + cgstcal;
	}
	var tot = (totdisAmt + totaltaxamt).toFixed(2);
	//   var totdisc = totdisAmt;

	$('.gst-' + id).html(gstspl[0]);
	$('.cgst-' + id).html(cgstspl[0]);
	$('.sgst-' + id).html(sgstspl[0]);

	$('.gstdisplay-' + id).html(gstcal.toFixed(2));
	$('.cgstdisplay-' + id).html(cgstcal.toFixed(2));
	$('.sgstdisplay-' + id).html(sgstcal.toFixed(2));


	$('.grandtotal' + id).html(tot);
	$('.grandtotaldisc' + id).html(totdisAmt.toFixed(2));

}

//DESCRESE QUANTITY COUNT
function decreaseValue(id, index) {
	//$('.grandtotal' + id + '').empty();
	var value = parseInt(document.getElementById('number-' + id + '').value, 10);
	value = isNaN(value) ? 0 : value;
	value--;
	if (value < 0) {
		mes = "Not Allowed";
		appdata.warningmsg(mes);
		return;
	}
	document.getElementById('number-' + id + '').value = value;
	// var data = $(".price-" + id).val();
	// var gst = $(".gst-" + id + "").text();
	// var cgst = $(".cgst-" + id + "").text();
	// var sgst = $(".sgst-" + id + "").text();
	// // var gstspl = gst.split("%");
	// var sgstspl = sgst.split("%");
	// var cgstspl = cgst.split("%");
	// var total = data * value;
	// // var gstcal = total * gstspl[0] / 100;
	// var sgstcal = total * sgstspl[0] / 100;
	// var cgstcal = total * cgstspl[0] / 100;
	// var tot = total +  sgstcal + cgstcal;
	// $('.grandtotal' + id + '').html(tot);

	var SGST = $(".sgst-" + id + "").text();
	var CGST = $(".cgst-" + id + "").text();
	var GST = $(".gst-" + id + "").text();

	var gstspl = GST.split("%");
	var sgstspl = SGST.split("%");
	var cgstspl = CGST.split("%");

	var gsttypeid = 0;
	var sgsttypeid = 0;
	var cgsttypeid = 0;
	var discount;
	if (index == '' || index == undefined) {
		discount = $('#disc').val();
	}
	else {
		discount = $('#disc' + index).val();
	}
	if (discount == '' || discount == undefined) {
		var disc = 0;
	} else {
		var disc = discount;
	}

	var total = $('#itemrate' + id).val() * value;
	var discAmt = (total * disc) / 100;
	var totdisAmt = total - discAmt;
	var gstcal = totdisAmt * gstspl[0] / 100;
	var sgstcal = totdisAmt * sgstspl[0] / 100;
	var cgstcal = totdisAmt * cgstspl[0] / 100;
	var totaltaxamt;
	if (gstspl[0] != 0 || gstspl[0] != 0.00) {
		totaltaxamt = gstcal;
	} else {
		totaltaxamt = sgstcal + cgstcal;
	}
	var tot = (totdisAmt + totaltaxamt).toFixed(2);
	//   var totdisc = totdisAmt ;

	$('.gst-' + id).html(gstspl[0]);
	$('.cgst-' + id).html(cgstspl[0]);
	$('.sgst-' + id).html(sgstspl[0]);

	$('.gstdisplay-' + id).html(gstcal.toFixed(2));
	$('.cgstdisplay-' + id).html(cgstcal.toFixed(2));
	$('.sgstdisplay-' + id).html(sgstcal.toFixed(2));


	$('.grandtotal' + id).html(tot);
	$('.grandtotaldisc' + id).html(totdisAmt.toFixed(2));
}
//dynamic item autocomplete
$(function () {
	var count = 1;
	$("#btn").click(function () {
		count++;
		$('#myTabContent').append('<div class="itemcontent-' + count + '" style="margin-top: 10px;"><div class="card innercard"><div class="row"><div class="col-sm-5"><p class="justcolor">Product Name:</p></div><div class="col-sm-7"><input id="disitem-' + count + '"  name=' + count + '  type="text" dir="123"    class="searchitem searchInput disitem"/><p class="empty-message"></p></div></div><div class="itemdetails-' + count + '"></div></div></div>');
		console.log($(".searchInput"));
		// $('#chkgst'+count).change(function() {
		// 	if($(this).is(":checked")){
		// 	  $('#chkigst'+count).attr('checked', false);
		// 	}    
		// });

		// $('#chkigst'+count).change(function() {
		// 	if($(this).is(":checked")){
		// 	  $('#chkgst'+count).attr('checked', false);
		// 	}    
		// });
		$(".searchInput").autocomplete({
			source: function (request, response) {
				var id = this.element[0].id;
				var val = $("#" + id).val();
				$('.loading').addClass('spinner');
				$.ajax({
					type: "POST",
					//contentType: "application/json",
					url: 'http://183.82.98.147:9000/api/getitemnames/',
					data: {
						company_id: localStorage.getItem("company_id")
					},
					dataType: "json",
					success: function (data) {

						itemdetails = data;
						var id = $(this).attr('id');
						$(this).removeClass('ui-autocomplete-loading');
						$('.loading').removeClass('spinner');
						response(itemdetails);
					},
					error: function (data) {
						$('#' + id).removeClass('ui-autocomplete-loading');
					}
				});
			},
			select: function (event, ui) { //SELECT DDL EVENT
				event.preventDefault();
				$("#disitem-" + event.target.name + "").val(ui.item.label);
				var index = event.target.name;


				// if($("#disitem-" + count).text() == $("#itemname").text()){
				// 	mes = "Please select another one";
				// 	appdata.warningmsg(mes);
				// 	$("#disitem-" + count).val('');
				// 	return;
				// }
				$('.loading').addClass('spinner');
				appdata.getitemsbyitemname1(ui.item.id, index);
				checkitemname.push(ui.item.id);
				$("#disitem-" + count + "").attr("disabled", "disabled");

			},
			minLength: 3
		});
	});
});
//pay to back button
$('.paytoback').click(function () {
	$('.showvendordtls').addClass('hide');
	$('.itemdetailsview').removeClass('hide');
	$('.itemdtl').removeClass('hide');
	$('.viewitemdtl').addClass('hide');
	$('.payitems').addClass('hide');
	$('.submititems').removeClass('hide');
	$('.viewdata').empty();
	appdata.getitemsdata();
});

//view select item list
$('.insertselectitems').click(function () {
	itemdetailsarray = [];
	alltaxarrays = [];
	qtyarray = [];
	totalarray = [];
	measurementarray = [];
	pricearray = [];
	runningstockarray = [];
	gstarray = [];
	cgstarray = [];
	sgstarray = [];
	itemtotalpricearray = [];
	itemidarray = [];
	gsttypeidarray = [];
	cgsttypeidarray = [];
	sgsttypeidarray = [];
	gstcalarray = [];
	cgstcalarray = [];
	sgstcalarray = [];
	totaltaxamt = [];
	quotations = [];
	$('.gstdata').each(function () {
		gstarray.push($(this).attr('data-tes'));
		console.log($(this).attr('data-tes'));

	});
	$('.cgstdata').each(function () {
		cgstarray.push($(this).attr('data-tes'));
		console.log($(this).attr('data-tes'));
	});
	$('.sgstdata').each(function () {
		sgstarray.push($(this).attr('data-tes'));
		console.log($(this).attr('data-tes'));
	});
	$('.datameasure').each(function () {
		measurementarray.push($(this).attr('data-tes'));
		console.log($(this).attr('data-tes'));
	});
	$('.itemprice').each(function () {
		pricearray.push($(this).attr('data-tes'));
	});
	$('.itemstock').each(function () {
		runningstockarray.push($(this).attr('data-tes'));
	});

	$("input[type='number'].qty").each(function () {
		qtyarray.push($(this).val());
	});
	$("input[type='text'].searchitem").each(function () {
		itemnames.push($(this).val());
		console.log(itemnames);
	});
	$('.itemtotalprice').each(function (event, ui) {
		var data = ui.innerHTML;
		itemtotalpricearray.push(data);
	});
	$('.itemid').each(function () {
		itemidarray.push($(this).attr('data-tes'));
	});

	$('.gsttypeid').each(function () {
		gsttypeidarray.push($(this).attr('data-tes'));
	});
	$('.cgsttypeid').each(function () {
		cgsttypeidarray.push($(this).attr('data-tes'));
	});

	$('.sgsttypeid').each(function () {
		sgsttypeidarray.push($(this).attr('data-tes'));
	});

	$('.totaltaxamt').each(function () {
		totaltaxamt.push($(this).attr('data-tes'));
	});

	//all tax cal amt
	$('.gstcal').each(function () {
		gstcalarray.push($(this).attr('data-tes'));
	});
	$('.sgstcal').each(function () {
		sgstcalarray.push($(this).attr('data-tes'));
	});
	$('.cgstcal').each(function () {
		cgstcalarray.push($(this).attr('data-tes'));
	});
	var leng = itemidarray.length;
	var name = itemnames.length;
	if (leng != name) {
		var mes = "Please select item name";
		appdata.warningmsg(mes);
		$('.loading').removeClass('spinner');
		return;
	}

	for (var i = 0; i < leng; i++) {
		alltaxarrays.push({
			"tax_type_id": 1,
			"percentage": 12,
			"tax_amount": 10,
		});
	}
	var gstallarray = [];
	for (var i = 0; i < leng; i++) {
		gstallarray.push({
			"tax_type_id": gsttypeidarray[i],
			"percentage": gstarray[i],
			"tax_amount": gstcalarray[i],
		});
		gstallarray.push({
			"tax_type_id": sgsttypeidarray[i],
			"percentage": sgstarray[i],
			"tax_amount": sgstcalarray[i],
		});
		gstallarray.push({
			"tax_type_id": cgsttypeidarray[i],
			"percentage": cgstarray[i],
			"tax_amount": cgstcalarray[i],
		});
		itemdetailsarray.push({
			"item_id": itemidarray[i],
			"quantity": qtyarray[i],
			"price": pricearray[i],
			"total_amount": itemtotalpricearray[i],
			"discount": 0,
			"discount_amount": 0,
			"item_taxes": gstallarray
		});
	}


	appdata.viewinsertitemdtls(itemdetailsarray, gstallarray, gstarray, sgstarray, cgstarray, measurementarray,
		pricearray, runningstockarray, qtyarray, itemtotalpricearray, itemidarray, gsttypeidarray,
		sgsttypeidarray, cgsttypeidarray, gstcalarray, sgstcalarray, cgstcalarray);
});

//SHOW ITEM DETAILS VIEW
$("#showitemdtls").click(function () {
	var selectedValue = selectedVendor;
	if (selectedValue == "" || selectedValue == NaN) {
		var mes = 'Please select vendor';
		appdata.warningmsg(mes); //DISPLAY MESSAGES
	} else {
		$('.showvendordtls').addClass('hide');
		$('.itemdetailsview').removeClass('hide');
		$('.vendor-details').removeClass('hide');
		$('.itemdtl').removeClass('hide');
		$('.submititems').removeClass('hide');



		appdata.getitemsdata();

	}
});
//END

function showitellistgstdata() {
	$('.itemlistshowdata').removeClass('hide');
	$('.hidelist').removeClass('hide');
	$('.showlist').addClass('hide');
}
function hideitellistgstdata() {
	$('.itemlistshowdata').addClass('hide');
	$('.hidelist').addClass('hide');
	$('.showlist').removeClass('hide');
}


//get item names in autocomplete
$(function () {
	$("#itemname").removeAttr("disabled");
	$(".searchitem").autocomplete({
		source: function (request, response) {
			$('.loading').addClass('spinner');
			$.ajax({
				type: "POST",
				//contentType: "application/json",
				url: 'http://127.0.0.1:8000/api/getitemnames/',
				data: {
					"company_id": localStorage.getItem("company_id")
				},
				dataType: "json",
				success: function (data) {
					//data = [{"label": "item1", "value": "item1"}, {"label": "item2", "value": "item1"}]
					itemdetails = data;
					var id = $(this).attr('id');
					$(this).removeClass('ui-autocomplete-loading');
					$('.loading').removeClass('spinner');
					response(itemdetails);
				},
				error: function (data) {
					$('#' + id).removeClass('ui-autocomplete-loading');
				}
			});
		},
		select: function (event, ui) {
			event.preventDefault();
			$("#itemname").val(ui.item.label);
			$('.loading').addClass('spinner');
			$.each(checkitemname, function (index, value) {
				console.log(value);
			});
			checkitemname.push(ui.item.id);

			appdata.getitemsbyitemname(ui.item.id);

			$("#itemname").attr("disabled", "disabled");

		},
		minLength: 3
	});

});

//SHOW ALL TAX CODE HERE
function showalltax(id) {
	$('.taxvisible').removeClass('hide');
	$('.hidemore').addClass('hide');
	$('.hidebtn').removeClass('hide');
	$('.alltaxs').removeClass('hide');
}
//TAX CODE END
//HIDE ALL TAX CODE HERE
function hidealltax(id) {
	$('.taxvisible').addClass('hide');
	$('.hidemore').removeClass('hide');
	$('.hidebtn').addClass('hide');
	$('.alltaxs').addClass('hide');
}
//TAX CODE END
//DELETE ITEM RECORD
function deleterecord(id) {
	$('.itemcontent-' + id + '').addClass('hide');
}
//DELETE ITEM RECORD CODE END

//SHOW FIRST RECORD CODE HERE
function showfirstrecord() {
	$('.alltaxs').removeClass('hide');
	$('.firstrecirdshow').addClass('hide');
	$('.firstrecirdhide').removeClass('hide');
}
//TAX CODE END
//HIDE FIRST RECORD CODE HERE
function hidefirstrecord() {
	$('.alltaxs').addClass('hide');
	$('.firstrecirdshow').removeClass('hide');
	$('.firstrecirdhide').addClass('hide');

}
//TAX CODE END
// FIRST RECORD DELETE ITEM RECORD
function deletefirstrecord() {
	$('.firstrecord').addClass('hide');
}
//DELETE ITEM RECORD CODE END

//change password page//
$("#chngpwd").on("click", function () {
	appdata.changepassword();
});
$("#showitemdtls1").on("click", function () {
	$('.purchase-payments').addClass('hide');
	$('.vendor-details').addClass('hide');
	$('.viewitemdtl').addClass('hide');
	$('.payitems').addClass('hide');
	$('.payments-list').addClass('hide');
	appdata.paymentslist();
});
body.on('click', '.putinnerdata', function (e) {
	$('.purhidedata').removeClass('hide');
});
function showtransdata(id) {
	$('.purhidedata-' + id + '').removeClass('hide');
}
function editpurchasesitems(id) {
	appdata.editpurchasesitems(id);
}
function paymentitem(invoiceid, balAmount) {
	localStorage.setItem('invoiceid', invoiceid);
	$('.purchase-payments').addClass('hide');
	$('.vendor-details').addClass('hide');
	$('.viewitemdtl').addClass('hide');
	$('.payitems').remove('hide');
	$('.payments-list').addClass('hide');
	$('.purchaselist').addClass('hide');
	$('.loading').addClass('spinner');
	appdata.paymentslist(invoiceid, balAmount);
}

$("#payListConfirm").on("click", function () {
	invoiceid = localStorage.getItem("invoiceid");
	$('.payments-list').addClass('hide');
	$('.purchase-payments').removeClass('hide');
	$(".loading").addClass("spinner");
	appdata.purchasepayments(invoiceid);
});

function delPayments(payList1, payment_id, paymeny_amount) {
	invoice_id = localStorage.getItem('invoiceid');
	var paydeldata = { "transaction_id": payList1, "payment_id": payment_id, "payment_amount": paymeny_amount };
	// $(".loading").addClass("spinner");
	api.post1('deletepayment/', paydeldata, function (data) {
		// $(".loading").removeClass("spinner");
		$(".loading").addClass("spinner");
		appdata.paymentslist(invoice_id);
		// $(".loading").removeClass("spinner");
	});
}

$('#purvendor').change(function () {
	purvendorid = parseInt(jQuery(this).val());
});

$('#txtPaymentAmount').keypress(function (event) {
	return isNumber(event, this)
});
function isNumber(evt, element) {

	var charCode = (evt.which) ? evt.which : event.keyCode

	if ((charCode < 48 || charCode > 57))
		return false;

	return true;
}

$("#backtoPurList").on("click", function () {
	// localStorage.setItem('invoiceid',invoiceid);
	invoiceid = localStorage.getItem("invoiceid");
	appdata.paymentslist(invoiceid);
	$('.purchase-payments').addClass('hide');
	$('.payments-list').addClass('hide');
});



$("#purListfromPay").on("click", function () {
	$('.payments-list').addClass('hide');
	$('.purchaselist').removeClass('hide');
});


$("#newpurchase").click(function () {
	$('.loading').addClass('spinner');
	// $(".loggedin").removeClass("hide");  
	$(".vendor-details").removeClass("hide"); //VENDOR DETAILS VISIBLE HERE
	$(".change-password").addClass("hide"); // change password page hide
	$(".profile-update").addClass("hide"); // profile update page hide
	$(".purchase-payments").addClass("hide");
	$(".login").addClass("hide"); // LOGIN PAGE HIDE HERE
	$(".purchaselist").addClass("hide");
	// $(".content").removeClass("login_page");
	$(".showvendordtls").removeClass("hide");



	$('#vendor').val(undefined);
	$('#mobileno').val("");
	$('#address').val("");
	$('#panno').val("");
	$('#aadharno').val("");

	$('.loading').removeClass('spinner');
});



$('#chkgst').change(function () {
	if ($(this).is(":checked")) {
		$('#chkigst').attr('checked', false);
	}
	if ($(this).is(":checked") == false) {
		$('#chkigst').attr('checked', false);
	}

});

$('#chkigst').change(function () {
	if ($('#chkgst').is(":checked")) {
		$('#chkgst').attr('checked', true);
	} else {
		$('#chkigst').attr('checked', false);
	}
});


// cash book 

$("#acctheadid").change(function () {
	var acthid = Number($('#acctheadid').val());
	// var acthid =$("#acctheadid")[0].selectedIndex;
	var acctobjname = {
		company_id: localStorage.getItem('company_id'),
		account_head_id: acthid
	}
	$('#acctheadname').empty();
	$('#acctheadname').append('<option value="">All</option>');

	if (acthid == 1 || acthid == 6) {
		$('#lbl-acctheadname').text('Customer Name');

	}
	else if (acthid == 2 || acthid == 5) {
		$('#lbl-acctheadname').text('Vendor Name');
	}
	else if (acthid == 4) {
		$('#lbl-acctheadname').text('Bank Account Name');

	}
	else if (acthid == 7) {
		$('#lbl-acctheadname').text('Lender Name');

	}
	else if (acthid == 3 || acthid == '') {
		$('#lbl-acctheadname').text('Account Head Name');
	}

	api.post1('getaccountheadnamebyheadid/', acctobjname, function (data) {
		groupheadname = (data)
		for (var i = 0; i < groupheadname.length; i++) {
			$('#acctheadname').append('<option value="' + groupheadname[i].account_id + '">' +
				groupheadname[i].account_name + '</option>');
		}
	})
}
)

$('#btnCashBook').click(function () {
	// $('#myTable').removeClass("hide");
	appdata.getcashbooktable();
})





