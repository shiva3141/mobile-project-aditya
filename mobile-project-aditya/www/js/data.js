var appState = [];
document.addEventListener("deviceready", init, false);
var appdata = new Object();

function init() {
  $(".content").addClass("login_page");
}
//loginform code here
appdata.loginform = function (email, password) {
  var data = {
    email: email,
    password: password
  };

  logindata = {
    params: data
  };

  api.post1('userlogin/', logindata, function (data) {
    logindetails = data;
    var length = logindetails.login_details.length;
    if (length != 0) {
      localStorage.setItem('company_id', logindetails.login_details[0]['company_id']);
      localStorage.setItem('email', logindetails.login_details[0]['email']);
      localStorage.setItem('first_name', logindetails.login_details[0]['first_name']);
      localStorage.setItem('user_id', logindetails.login_details[0]['user_id']);
      localStorage.setItem('role_id', logindetails.login_details[0]['role_id']);
      var mes = 'Login Successfully...!';
      appdata.successmsg(mes); //DISPLAY MESSAGES
      $('.loading').addClass('spinner'); // HERE LOADER VISIBLE
      $(".loggedin").removeClass("hide"); // HEADER HIDE HERE
      $(".vendor-details").removeClass("hide"); //VENDOR DETAILS VISIBLE HERE
      $(".change-password").addClass("hide"); // change password page hide
      $(".profile-update").addClass("hide"); // profile update page hide
      $(".purchase-payments").addClass("hide");
      $(".login").addClass("hide"); // LOGIN PAGE HIDE HERE
      $(".content").removeClass("login_page");
      $('.loading').removeClass('spinner'); //HERE LOADER HIDE
    } else {
      var mes = 'Invalid userid and password';
      appdata.errormsg(mes); //DISPLAY MESSAGES
      $('.loading').removeClass('spinner'); //HERE LOADER HIDE
      $('.loading').addClass('spinner'); // HERE LOADER VISIBLE
      $(".loggedin").removeClass("hide"); // HEADER HIDE HERE
      $(".vendor-details").removeClass("hide"); //VENDOR DETAILS VISIBLE HERE
      $(".login").addClass("hide"); // LOGIN PAGE HIDE HERE
      $(".content").removeClass("login_page");
      $('.loading').removeClass('spinner');
    }
  });
};
//end

appdata.getuserscompanylist = function (email_id) {
  var user_cmpy = [];
  var emailobj = {
    email: email_id
  }
  var companylist = [];
  var user_companies = [];
  api.post('getcompanies/', '', function (data) {
    companylist = $.parseJSON(data);
    api.post('getusercompanylist/', emailobj, function (res) {
      user_companies = $.parseJSON(res);

      if (user_companies.length == 1) {
        var a;
        if (companylist[0].company_id === user_companies[0].company_id) {
          a = companylist[0];
          $('#purchase_company_id').append('<option value="' + a.company_id + '">' + a.company_name + '</option>');

        }
        if (user_companies[0].company_id === 1) {
          a = companylist;
          for (var k = 0; k < a.length; k++) {
            $('#purchase_company_id').append('<option value="' + a[k].company_id + '">' + a[k].company_name + '</option>');
          }
        }
      }
      else {
        for (var i = 0; i < user_companies.length; i++) {
          for (var k = 0; k < companylist.length; k++) {
            if (user_companies[i].company_id == companylist[k].company_id) {
              $('#purchase_company_id').append('<option value="' + companylist[k].company_id + '">' + companylist[k].company_name + '</option>');
            }
          }
        }
      }

    })
  });

}


//Get Vendors
appdata.getvendor = function () {

  var data = {
    company_id: localStorage.getItem('company_id')
  };

  api.post('getvendorsforpurchase/', data, function (data) {
    $('.loading').removeClass('spinner');
    var vendordata = $.parseJSON(data);
    $.each(vendordata, function (index, values) {
      $('#vendor').append('<option value="' + values.vendor_id + '">' + values.vendor_name + '</option>');
    });

  });
};

//get purchare vendors data
appdata.getpurvendor = function () {
  $("#purvendor option").remove();
  var data = {
    company_id: localStorage.getItem('company_id')
  };
  api.post('getvendorsforpurchase/', data, function (data) {
    var vendordata = $.parseJSON(data);
    $.each(vendordata, function (index, values) {
      $('#purvendor').append('<option value="' + values.vendor_id + '">' + values.vendor_name + '</option>');
    });

  });
};

//get companys data
appdata.getcompanys = function () {
  $("#purcompany option").remove();
  var data = {
    company_id: localStorage.getItem('company_id')
  };
  api.post('getcompanies/', data, function (data) {
    var companydata = $.parseJSON(data);
    $.each(companydata, function (index, values) {
      $('#purcompany').append('<option value="' + values.company_id + '">' + values.company_name + '</option>');
    });

  });
};


//get vendor details by vendor id
appdata.getvendordetails = function (vendorid) {
  var getvendorid = {
    "vendor_id": vendorid
  };
  var data = {
    vendor_id: vendorid
  };
  getvendordtlbyvendorid = {
    params: data
  };
  api.post1('getvendordetails/', getvendordtlbyvendorid, function (data) {
    $('.loading').removeClass('spinner');
    var vendordata = data;
    var mobileno = vendordata[0]['phone'];
    var address = vendordata[0]['address1'];
    var gstin = vendordata[0]['gstin'];
    $('#mobileno').val(mobileno);
    $('#address').val(address);
    $('#aadharno').val(gstin);

  });
};


//success messages
appdata.successmsg = function (msg) {
  $('#msg').empty();
  $('.alertMsg').removeClass('hide');
  $('.alertMsg').addClass('sucessMsg');
  $('#msg').append(msg);
  $("#success-alert").show();
  setTimeout(function () {
    $("#success-alert").hide();
  }, 500);
};

//warning messages
appdata.warningmsg = function (msg) {
  $('#msg').empty();
  $('.alertMsg').removeClass('hide');
  $('.alertMsg').addClass('warningMsg');
  $('#msg').append(msg);
  $("#success-alert").show();
  setTimeout(function () {
    $("#success-alert").hide();
  }, 500);
};

//warning messages
appdata.warningmsg_pwdchng = function (msg) {
  $('#msg').empty();
  $('.alertMsg').removeClass('hide');
  $('.alertMsg').addClass('warningMsg');
  $('#msg').append(msg);
  $("#success-alert").show();
  setTimeout(function () {
    $("#success-alert").hide();
  }, 2000);
};

//error messages
appdata.errormsg = function (msg) {
  $('#msg').empty();
  $('.alertMsg').removeClass('hide');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append(msg);
  $("#success-alert").show();
  setTimeout(function () {
    $("#success-alert").hide();
  }, 500);
};

//GET ITEMS BY ITEM NAME
appdata.getitemsbyitemname = function (itemname) {
  var companyid = localStorage.getItem('company_id');
  var itemname = itemname;
  var getitemdtlbyitemname = {
    company_id: companyid,
    item_id: itemname
  };


  api.post('getitemdetailsbyname/', getitemdtlbyitemname, function (data) {
    var itemhtml = '';
    itemdetails = $.parseJSON(data);

    var index = null;

    var unit_of_measurement = itemdetails[0]['unit_of_measurement'];
    var price = itemdetails[0]['price'];


    var available_quantity = itemdetails[0]['available_quantity'];
    var item_code = itemdetails[0]['item_code'];
    var item_id = itemdetails[0]['item_id'];
    var item_name = itemdetails[0]['item_name'];
    var tax = itemdetails[0]['item_taxes'];
    var len = tax.length;

    var SGST = 0.00;
    var CGST = 0.00;
    var GST = 0.00;
    var gsttypeid = 0;
    var sgsttypeid = 0;
    var cgsttypeid = 0;


    if ($('#chkgst').is(":checked") === true && $('#chkigst').is(":checked") === false) {
      if (len != 0) {
        for (i = 0; i <= len - 1; i++) {
          var type = tax[i]['tax_type'];
          if (type == 'SGST') {
            SGST = tax[i]['tax_percentage'];
            sgsttypeid = tax[i]['tax_type_id'];
          } else if (type == 'CGST') {
            CGST = tax[i]['tax_percentage'];
            cgsttypeid = tax[i]['tax_type_id'];
          } else {
            GST = 0.00;
            gsttypeid = tax[i]['tax_type_id'];
          }
        }
      }
    }

    if ($('#chkgst').is(":checked") === true && $('#chkigst').is(":checked") === true) {
      if (len != 0) {
        for (i = 0; i <= len - 1; i++) {
          var type = tax[i]['tax_type'];
          if (type == 'SGST') {
            SGST = 0.00;
            sgsttypeid = tax[i]['tax_type_id'];
          } else if (type == 'CGST') {
            CGST = 0.00;
            cgsttypeid = tax[i]['tax_type_id'];
          } else {
            GST = tax[i]['tax_percentage'];
            gsttypeid = tax[i]['tax_type_id'];
          }
        }
      }
    }

    if ($('#chkgst').is(":checked") === false && $('#chkigst').is(":checked") === false) {
      if (len != 0) {
        for (i = 0; i <= len - 1; i++) {
          var type = tax[i]['tax_type'];
          if (type == 'SGST') {
            SGST = 0.00;
            sgsttypeid = tax[i]['tax_type_id'];
          } else if (type == 'CGST') {
            CGST = 0.00;
            cgsttypeid = tax[i]['tax_type_id'];
          } else {
            GST = 0.00;
            gsttypeid = tax[i]['tax_type_id'];
          }
        }
      }
    }

    var chkgst = $('#chkgst').is(":checked");
    var chkigst = $('#chkigst').is(":checked");

    // if (len != 0) {
    //   for (i = 0; i <= len - 1; i++) {
    //     var type = tax[i]['tax_type'];
    //     if (type == 'SGST') {
    //       SGST = tax[i]['tax_percentage'];
    //       sgsttypeid = tax[i]['tax_type_id'];
    //     } else if (type == 'CGST') {
    //       CGST = tax[i]['tax_percentage'];
    //       cgsttypeid = tax[i]['tax_type_id'];
    //     } else {
    //       GST = tax[i]['tax_percentage'];
    //       gsttypeid = tax[i]['tax_type_id'];
    //     }
    //   }
    // }


    var total = price * 1;
    var gstcal = total * GST / 100;
    var sgstcal = total * SGST / 100;
    var cgstcal = total * CGST / 100;
    var totaltaxamt = sgstcal + cgstcal;
    var tot = (total + totaltaxamt).toFixed(2);
    var totdisc = total.toFixed(2);

    gstcal = ((totdisc * GST) / 100).toFixed(2);
    sgstcal = ((totdisc * SGST) / 100).toFixed(2);
    cgstcal = ((totdisc * CGST) / 100).toFixed(2);

    itemhtml += '<div class="row">';
    itemhtml += '<div class="col-sm-5"><p class="justcolor">Price:</p></div>';
    itemhtml += '<div class="col-sm-7"><input id="itemrate' + item_id + '" type="text" class="itemprice price-' + item_id + '" data-tes="' + price + '"  value="' + price + '" ><p class="empty-message"></p></div></div>';



    itemhtml += '<div class="row">';
    itemhtml += '<div class="col-md-6">';
    itemhtml += '<p class="hide"><span class="itemid" data-tes=' + item_id + '>' + item_id + '</span></p>';
    itemhtml += '<p class="hide"><span class="totaltaxamt gst-' + totaltaxamt + '" data-tes=' + totaltaxamt + '>' + totaltaxamt + '</span></p>';

    //tax types ids
    itemhtml += '<p class="hide"><span class="gsttypeid" data-tes=' + gsttypeid + '>' + gsttypeid + '</span></p>';
    itemhtml += '<p class="hide"><span class="sgsttypeid" data-tes=' + sgsttypeid + '>' + sgsttypeid + '</span></p>';
    itemhtml += '<p class="hide"><span class="cgsttypeid" data-tes=' + cgsttypeid + '>' + cgsttypeid + '</span></p>';
    //each tax calculatation amt
    itemhtml += '<p class="hide"><span class="gstcal" data-tes=' + gstcal + '>' + gstcal + '</span></p>';
    itemhtml += '<p class="hide"><span class="sgstcal" data-tes=' + sgstcal + '>' + sgstcal + '</span></p>';
    itemhtml += '<p class="hide"><span class="cgstcal" data-tes=' + cgstcal + '>' + cgstcal + '</span></p>';



    itemhtml += '<p class="justcolor">Quantity</p>';
    itemhtml += '<div class="plusminus">';
    itemhtml += '<div class="value-button valminus" id="decrease" onclick="decreaseValue(' + item_id + ',' + index + ')" value="Decrease Value">-</div>';
    itemhtml += '<input type="number" id="number-' + item_id + '" value="1" class="qty displaycount" />';
    itemhtml += '<div class="value-button valplus" id="increase" onclick="increaseValue(' + item_id + ',' + index + ',' + available_quantity + ')" value="Increase Value">+</div>';
    itemhtml += '<div class="clear"></div>';
    itemhtml += '</div>';

    itemhtml += '</div>';
    itemhtml += '<div class="col-md-6">';
    itemhtml += '<p class="justcolor">Measurement  <span class="datameasure measurement-' + item_id + '" data-tes=' + unit_of_measurement + '>' + unit_of_measurement + '</span></p>';
    // itemhtml += '<p class="measuresty">Price<span class="itemprice datameasure1 price-' + item_id + '" data-tes=' + price + '>' + price + '</span></p>';
    itemhtml += '</div>';
    itemhtml += '</div>';
    itemhtml += '<div class="row"><div class="col-md-6"><p class="measuresty">Running Stock<span class="itemstock datameasure1 runqty-' + item_id + '" data-tes=' + available_quantity + '>' + available_quantity + '</span></p></div><div class="col-md-6"><span class="justcolor dist-width">Discount(%)<input id="disc" type="text"   class="txtwidth" ><div class="clear"></div></span></div></div>';

    itemhtml += '<div class="alltaxs hide">';
    itemhtml += '<div class="row">';
    itemhtml += '<div class="col-sm-4">';
    itemhtml += '<p>IGST</p>';
    itemhtml += '<p class="gstdata gst-' + item_id + '" data-tes=' + GST + '>' + GST + '%' + '</p>';
    itemhtml += '<p class="gstdisplay-' + item_id + '">' + gstcal + '</p>';
    itemhtml += '</div>';
    itemhtml += '<div class="col-sm-4">';
    itemhtml += '<p>CGST</p>';
    itemhtml += '<p class="cgstdata cgst-' + item_id + '" data-tes=' + CGST + '>' + CGST + '%' + '</p>';
    itemhtml += '<p class="cgstdisplay-' + item_id + '">' + cgstcal + '</p>';
    itemhtml += '</div>';
    itemhtml += '<div class="col-sm-4">';
    itemhtml += '<p>SGST</p>';
    itemhtml += '<p class="sgstdata sgst-' + item_id + '" data-tes=' + SGST + '>' + SGST + '%' + '</p>';
    itemhtml += '<p class="sgstdisplay-' + item_id + '">' + sgstcal + '</p>';
    itemhtml += '</div>';
    itemhtml += '</div>';
    itemhtml += '</div>';
    itemhtml += '</div>';



    itemhtml += '<div class="row">';
    itemhtml += '<div class="col-sm-5">';
    itemhtml += '<p class="justcolor">Total:<span class="itemtotalprice boldcls grandtotal' + item_id + '" data-tes=' + tot + '>' + tot + '</span></p>';
    itemhtml += '</div>';
    itemhtml += '<div class="col-sm-7">';
    itemhtml += '<p class="justcolor">After Discount:<span class="itemtotalprice boldcls grandtotaldisc' + item_id + '" data-tes=' + totdisc + '>' + totdisc + '</span></p>';
    itemhtml += '</div>';
    itemhtml += '</div>';

    itemhtml += '<div class="row">';
    itemhtml += '<div class="col-sm-5">';
    itemhtml += '</div>';
    itemhtml += '<div class="col-sm-7">';
    itemhtml += '<div class="next-btn firstrecirdshow" onclick="showfirstrecord()" style="margin-left:15px;" id="showitemdtls">';
    itemhtml += '<a>more</a>';
    itemhtml += '</div>';
    itemhtml += '<div class="next-btn firstrecirdhide hide" style="margin-left:15px;" onclick="hidefirstrecord()" id="showitemdtls" onclick="editpurchasesitems()">';
    itemhtml += '<a>Hide</a>';
    itemhtml += '</div>';
    itemhtml += '<div class="next-btn" id="showitemdtls" onclick="deletefirstrecord()">';
    itemhtml += '<a>Delete</a>';
    itemhtml += '</div>';
    itemhtml += '</div>';
    itemhtml += '</div>';



    $('.itemdetails').append(itemhtml);
    $('.loading').removeClass('spinner');



    $("#itemrate" + item_id).on("keyup", function () {
      calculateTotals(index, item_id, tax, len, chkgst, chkigst);

    });

    $("#number-" + item_id).on("keyup", function () {
      if ($("#number-" + item_id).val() <= available_quantity) {
        calculateTotals(index, item_id, tax, len, chkgst, chkigst);
      }
      else {
        mes = "More than running stock is not allowed";
        appdata.warningmsg(mes);
        $("#number-" + item_id).val('');
        return;
      }
    });


    // function calculateTotals() {
    //   var SGST = 0.00;
    //   var CGST = 0.00;
    //   var GST = 0.00;
    //   var gsttypeid = 0;
    //   var sgsttypeid = 0;
    //   var cgsttypeid = 0;
    //   var discount = $('#disc').val();
    //   if (discount == '' || discount == undefined) {
    //     var disc = 0;
    //   } else {
    //     var disc = discount;
    //   }

    //   if (len != 0) {
    //     for (i = 0; i <= len - 1; i++) {
    //       var type = tax[i]['tax_type'];
    //       if (type == 'SGST') {
    //         SGST = tax[i]['tax_percentage'];
    //         sgsttypeid = tax[i]['tax_type_id'];
    //       } else if (type == 'CGST') {
    //         CGST = tax[i]['tax_percentage'];
    //         cgsttypeid = tax[i]['tax_type_id'];
    //       } else {
    //         GST = tax[i]['tax_percentage'];
    //         gsttypeid = tax[i]['tax_type_id'];
    //       }
    //     }
    //   }
    //   var total = $('#itemrate1').val() * $("#number-" + item_id).val();
    //   var discAmt = (total * disc) / 100;
    //   var totdisAmt = total - discAmt;
    //   // var gstcal = total * GST / 100;
    //   var sgstcal = totdisAmt * SGST / 100;
    //   var cgstcal = totdisAmt * CGST / 100;
    //   var totaltaxamt = sgstcal + cgstcal;
    //   var tot = (totdisAmt+ totaltaxamt).toFixed(2);
    //   // var totdisc = totdisAmt  ;
    //    $('.grandtotal' + item_id).html(tot);
    //    $('.grandtotaldisc' + item_id).html(totdisAmt.toFixed(2));
    // }

    $("#disc").on("keyup", function () {
      calculateTotals(index, item_id, tax, len, chkgst, chkigst);

    });

    $('#chkgst').change(function () {
      if ($(this).is(":checked")) {
        $('#chkigst').attr('checked', false);
      }
      if ($(this).is(":checked") == false) {
        $('#chkigst').attr('checked', false);
      }
      var chkgst = $('#chkgst').is(":checked");
      var chkigst = $('#chkigst').is(":checked");
      calculateTotals(index, item_id, tax, len, chkgst, chkigst);
    });

    $('#chkigst').change(function () {
      if ($('#chkgst').is(":checked")) {
        $('#chkgst').attr('checked', true);
      } else {
        $('#chkigst').attr('checked', false);
      }
      var chkgst = $('#chkgst').is(":checked");
      var chkigst = $('#chkigst').is(":checked");
      calculateTotals(index, item_id, tax, len, chkgst, chkigst);
    });



  });

}

//GET ITEMS BY ITEM NAME
appdata.getitemsbyitemname1 = function (itemid, index) {
  var companyid = localStorage.getItem('company_id');
  var getitemdtlbyitemname = {
    company_id: companyid,
    item_id: itemid
  };

  api.post('getitemdetailsbyname/', getitemdtlbyitemname, function (data) {
    var itemhtml = '';
    itemdetails = $.parseJSON(data);


    var unit_of_measurement = itemdetails[0]['unit_of_measurement'];
    var price = itemdetails[0]['price'];


    var available_quantity = itemdetails[0]['available_quantity'];
    var item_code = itemdetails[0]['item_code'];
    var item_id = itemdetails[0]['item_id'];
    var item_name = itemdetails[0]['item_name'];
    var tax = itemdetails[0]['item_taxes'];
    var len = tax.length;

    var SGST = 0.00;
    var CGST = 0.00;
    var GST = 0.00;
    var gsttypeid = 0;
    var sgsttypeid = 0;
    var cgsttypeid = 0;
    // if (len != 0) {
    //   for (i = 0; i <= len - 1; i++) {
    //     var type = tax[i]['tax_type'];
    //     if (type == 'SGST') {
    //       SGST = tax[i]['tax_percentage'];
    //       sgsttypeid = tax[i]['tax_type_id'];
    //     } else if (type == 'CGST') {
    //       CGST = tax[i]['tax_percentage'];
    //       cgsttypeid = tax[i]['tax_type_id'];
    //     } else {
    //       GST = tax[i]['tax_percentage'];
    //       gsttypeid = tax[i]['tax_type_id'];
    //     }
    //   }
    // }

    if ($('#chkgst').is(":checked") === true && $('#chkigst').is(":checked") === false) {
      if (len != 0) {
        for (i = 0; i <= len - 1; i++) {
          var type = tax[i]['tax_type'];
          if (type == 'SGST') {
            SGST = tax[i]['tax_percentage'];
            sgsttypeid = tax[i]['tax_type_id'];
          } else if (type == 'CGST') {
            CGST = tax[i]['tax_percentage'];
            cgsttypeid = tax[i]['tax_type_id'];
          } else {
            GST = 0.00;
            gsttypeid = tax[i]['tax_type_id'];
          }
        }
      }
    }

    if ($('#chkgst').is(":checked") === true && $('#chkigst').is(":checked") === true) {
      if (len != 0) {
        for (i = 0; i <= len - 1; i++) {
          var type = tax[i]['tax_type'];
          if (type == 'SGST') {
            SGST = 0.00;
            sgsttypeid = tax[i]['tax_type_id'];
          } else if (type == 'CGST') {
            CGST = 0.00;
            cgsttypeid = tax[i]['tax_type_id'];
          } else {
            GST = tax[i]['tax_percentage'];
            gsttypeid = tax[i]['tax_type_id'];
          }
        }
      }
    }

    if ($('#chkgst').is(":checked") === false && $('#chkigst').is(":checked") === false) {
      if (len != 0) {
        for (i = 0; i <= len - 1; i++) {
          var type = tax[i]['tax_type'];
          if (type == 'SGST') {
            SGST = 0.00;
            sgsttypeid = tax[i]['tax_type_id'];
          } else if (type == 'CGST') {
            CGST = 0.00;
            cgsttypeid = tax[i]['tax_type_id'];
          } else {
            GST = 0.00;
            gsttypeid = tax[i]['tax_type_id'];
          }
        }
      }
    }

    var chkgst = $('#chkgst').is(":checked");
    var chkigst = $('#chkigst').is(":checked");

    var total = price * 1;
    var gstcal = total * GST / 100;
    var sgstcal = total * SGST / 100;
    var cgstcal = total * CGST / 100;
    var totaltaxamt = sgstcal + cgstcal;
    var tot = (total + totaltaxamt).toFixed(2);
    var totdisc = total.toFixed(2);

    itemhtml += '<div class="row">';
    itemhtml += '<div class="col-sm-5"><p class="justcolor">Price:</p></div>';
    itemhtml += '<div class="col-sm-7"><input id="itemrate' + item_id + '" type="text" class="itemprice price-' + item_id + '" data-tes="' + price + '"  value="' + price + '" ><p class="empty-message"></p></div></div>';



    itemhtml += '<div class="row">';
    itemhtml += '<div class="col-md-6">';
    itemhtml += '<p class="hide"><span class="itemid" data-tes=' + item_id + '>' + item_id + '</span></p>';
    itemhtml += '<p class="hide"><span class="totaltaxamt gst-' + totaltaxamt + '" data-tes=' + totaltaxamt + '>' + totaltaxamt + '</span></p>';

    //tax types ids
    itemhtml += '<p class="hide"><span class="gsttypeid" data-tes=' + gsttypeid + '>' + gsttypeid + '</span></p>';
    itemhtml += '<p class="hide"><span class="sgsttypeid" data-tes=' + sgsttypeid + '>' + sgsttypeid + '</span></p>';
    itemhtml += '<p class="hide"><span class="cgsttypeid" data-tes=' + cgsttypeid + '>' + cgsttypeid + '</span></p>';
    //each tax calculatation amt
    itemhtml += '<p class="hide"><span class="gstcal" data-tes=' + gstcal + '>' + gstcal + '</span></p>';
    itemhtml += '<p class="hide"><span class="sgstcal" data-tes=' + sgstcal + '>' + sgstcal + '</span></p>';
    itemhtml += '<p class="hide"><span class="cgstcal" data-tes=' + cgstcal + '>' + cgstcal + '</span></p>';



    itemhtml += '<p class="justcolor">Quantity</p>';
    itemhtml += '<div class="plusminus">';
    itemhtml += '<div class="value-button valminus" id="decrease" onclick="decreaseValue(' + item_id + ',' + index + ')" value="Decrease Value">-</div>';
    itemhtml += '<input type="number" id="number-' + item_id + '" value="1" class="qty displaycount" />';
    itemhtml += '<div class="value-button valplus" id="increase" onclick="increaseValue(' + item_id + ',' + index + ',' + available_quantity + ')" value="Increase Value">+</div>';
    itemhtml += '<div class="clear"></div>';
    itemhtml += '</div>';

    itemhtml += '</div>';
    itemhtml += '<div class="col-md-6">';
    itemhtml += '<p class="justcolor">Measurement  <span class="datameasure measurement-' + item_id + '" data-tes=' + unit_of_measurement + '>' + unit_of_measurement + '</span></p>';
    // itemhtml += '<p class="measuresty">Price<span class="itemprice datameasure1 price-' + item_id + '" data-tes=' + price + '>' + price + '</span></p>';
    itemhtml += '</div>';
    itemhtml += '</div>';
    itemhtml += '<div class="row"><div class="col-md-6"><p class="measuresty">Running Stock<span class="itemstock datameasure1 runqty-' + item_id + '" data-tes=' + available_quantity + '>' + available_quantity + '</span></p></div><div class="col-md-6"><span class="justcolor dist-width">Discount(%)<input id="disc' + index + '"  type="text"   class="txtwidth" ><div class="clear"></div></span></div></div>';

    itemhtml += '<div class="alltaxs hide">';
    itemhtml += '<div class="row">';
    itemhtml += '<div class="col-sm-4">';
    itemhtml += '<p>IGST</p>';
    itemhtml += '<p class="gstdata gst-' + item_id + '" data-tes=' + GST + '>' + GST + '%' + '</p>';
    itemhtml += '<p class="gstdisplay-' + item_id + '">' + gstcal + '</p>';
    itemhtml += '</div>';
    itemhtml += '<div class="col-sm-4">';
    itemhtml += '<p>CGST</p>';
    itemhtml += '<p class="cgstdata cgst-' + item_id + '" data-tes=' + CGST + '>' + CGST + '%' + '</p>';
    itemhtml += '<p class="cgstdisplay-' + item_id + '">' + cgstcal + '</p>';
    itemhtml += '</div>';
    itemhtml += '<div class="col-sm-4">';
    itemhtml += '<p>SGST</p>';
    itemhtml += '<p class="sgstdata sgst-' + item_id + '" data-tes=' + SGST + '>' + SGST + '%' + '</p>';
    itemhtml += '<p class="sgstdisplay-' + item_id + '">' + sgstcal + '</p>';
    itemhtml += '</div>';
    itemhtml += '</div>';
    itemhtml += '</div>';
    itemhtml += '</div>';



    itemhtml += '<div class="row">';
    itemhtml += '<div class="col-sm-5">';
    itemhtml += '<p class="justcolor">Total:<span class="itemtotalprice boldcls grandtotal' + item_id + '" data-tes=' + tot + '>' + tot + '</span></p>';
    itemhtml += '</div>';
    itemhtml += '<div class="col-sm-7">';
    itemhtml += '<p class="justcolor">After Discount:<span class="itemtotalprice boldcls grandtotaldisc' + item_id + '" data-tes=' + totdisc + '>' + totdisc + '</span></p>';
    itemhtml += '</div>';
    itemhtml += '</div>';



    itemhtml += '<div class="row">';
    itemhtml += '<div class="col-sm-5">';
    itemhtml += '</div>';
    itemhtml += '<div class="col-sm-7">';
    itemhtml += '<div class="next-btn hidemore" onclick="showalltax(' + item_id + ')" style="margin-left:15px;" id="showitemdtls">';
    itemhtml += '<a>more</a>';
    itemhtml += '</div>';
    itemhtml += '<div class="next-btn hidebtn hide" style="margin-left:15px;" onclick="hidealltax(' + item_id + ')" id="showitemdtls" onclick="editpurchasesitems()">';
    itemhtml += '<a>Hide</a>';
    itemhtml += '</div>';
    itemhtml += '<div class="next-btn" id="showitemdtls" onclick="deleterecord(' + index + ')">';
    itemhtml += '<a>Delete</a>';
    itemhtml += '</div>';
    itemhtml += '</div>';
    itemhtml += '</div>';

    $('.itemdetails-' + index + '').append(itemhtml);
    $('.loading').removeClass('spinner');


    $("#itemrate" + item_id).on("keyup", function () {
      calculateTotals(index, item_id, tax, len, chkgst, chkigst);

    });

    $("#number-" + item_id).on("keyup", function () {
      calculateTotals(index, item_id, tax, len, chkgst, chkigst);
    });




    $("#disc" + index).on("keyup", function () {
      calculateTotals(index, item_id, tax, len, chkgst, chkigst);

    });

    $('#chkgst').change(function () {
      if ($(this).is(":checked")) {
        $('#chkigst').attr('checked', false);
      }
      if ($(this).is(":checked") == false) {
        $('#chkigst').attr('checked', false);
      }
      var chkgst = $('#chkgst').is(":checked");
      var chkigst = $('#chkigst').is(":checked");
      calculateTotals(index, item_id, tax, len, chkgst, chkigst);
    });

    $('#chkigst').change(function () {
      if ($('#chkgst').is(":checked")) {
        $('#chkgst').attr('checked', true);
      } else {
        $('#chkigst').attr('checked', false);
      }
      var chkgst = $('#chkgst').is(":checked");
      var chkigst = $('#chkigst').is(":checked");
      calculateTotals(index, item_id, tax, len, chkgst, chkigst);
    });

  });
};
function calculateTotals(index, item_id, tax, len, chkgst, chkigst) {
  var SGST = 0.00;
  var CGST = 0.00;
  var GST = 0.00;
  var gsttypeid = 0;
  var sgsttypeid = 0;
  var cgsttypeid = 0;
  var discount;
  if (index == '' || index == undefined) {
    discount = $('#disc').val();
  }
  else {
    discount = $('#disc' + index).val();
  }
  if (discount == '' || discount == undefined) {
    var disc = 0;
  } else {
    var disc = discount;
  }

  // if (len != 0) {
  //   for (i = 0; i <= len - 1; i++) {
  //     var type = tax[i]['tax_type'];
  //     if (type == 'SGST') {
  //       SGST = tax[i]['tax_percentage'];
  //       sgsttypeid = tax[i]['tax_type_id'];
  //     } else if (type == 'CGST') {
  //       CGST = tax[i]['tax_percentage'];
  //       cgsttypeid = tax[i]['tax_type_id'];
  //     } else {
  //       GST = tax[i]['tax_percentage'];
  //       gsttypeid = tax[i]['tax_type_id'];
  //     }
  //   }
  // }

  if (chkgst === true && chkigst === false) {
    if (len != 0) {
      for (i = 0; i <= len - 1; i++) {
        var type = tax[i]['tax_type'];
        if (type == 'SGST') {
          SGST = tax[i]['tax_percentage'];
          sgsttypeid = tax[i]['tax_type_id'];
        } else if (type == 'CGST') {
          CGST = tax[i]['tax_percentage'];
          cgsttypeid = tax[i]['tax_type_id'];
        } else {
          GST = 0.00;
          gsttypeid = tax[i]['tax_type_id'];
        }
      }
    }
  }

  if (chkgst === true && chkigst === true) {
    if (len != 0) {
      for (i = 0; i <= len - 1; i++) {
        var type = tax[i]['tax_type'];
        if (type == 'SGST') {
          SGST = 0.00;
          sgsttypeid = tax[i]['tax_type_id'];
        } else if (type == 'CGST') {
          CGST = 0.00;
          cgsttypeid = tax[i]['tax_type_id'];
        } else {
          GST = tax[i]['tax_percentage'];
          gsttypeid = tax[i]['tax_type_id'];
        }
      }
    }
  }

  if (chkgst === false && chkigst === false) {
    if (len != 0) {
      for (i = 0; i <= len - 1; i++) {
        var type = tax[i]['tax_type'];
        if (type == 'SGST') {
          SGST = 0.00;
          sgsttypeid = tax[i]['tax_type_id'];
        } else if (type == 'CGST') {
          CGST = 0.00;
          cgsttypeid = tax[i]['tax_type_id'];
        } else {
          GST = 0.00;
          gsttypeid = tax[i]['tax_type_id'];
        }
      }
    }
  }

  var total = $('#itemrate' + item_id).val() * $("#number-" + item_id).val();
  var discAmt = (total * disc) / 100;
  var totdisAmt = total - discAmt;
  var gstcal = totdisAmt * GST / 100;
  var sgstcal = totdisAmt * SGST / 100;
  var cgstcal = totdisAmt * CGST / 100;
  var totaltaxamt;
  if (GST != 0 || GST != 0.00) {
    totaltaxamt = gstcal;

  } else {
    totaltaxamt = sgstcal + cgstcal;
  }
  var tot = (totdisAmt + totaltaxamt).toFixed(2);
  // var totdisc = totdisAmt  ;

  $('.gst-' + item_id).html(GST);
  $('.cgst-' + item_id).html(CGST);
  $('.sgst-' + item_id).html(SGST);

  $('.gstdisplay-' + item_id).html(gstcal.toFixed(2));
  $('.cgstdisplay-' + item_id).html(cgstcal.toFixed(2));
  $('.sgstdisplay-' + item_id).html(sgstcal.toFixed(2));


  $('.grandtotal' + item_id).html(tot);
  $('.grandtotaldisc' + item_id).html(totdisAmt.toFixed(2));
}



appdata.getitemsdata = function () {
  var companyid = localStorage.getItem('company_id');
  var getitemsdata = {
    'company_id': companyid
  };
  api.post('getitemnames/', getitemsdata, function (data) {
    localStorage.setItem('itemjsondata', data);
  });
};

function formatDate(date) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('-');
}

//insert item details
appdata.insertitemdtls = function (itemdetailsarray, alltaxarrays, quantitytotal, vendorid, totaliteamt, totaltaxamt1, eachitemprice) {
  var companyid = localStorage.getItem('company_id');

  var data = {
    account_head_id: 2,
    transaction_type: 2,
    total_quantity: quantitytotal,
    total_item_amount: eachitemprice,
    overall_discount_percentage: 0,
    total_discount_amount: 0,
    total_tax_amount: totaltaxamt1,
    total_amount: totaliteamt,
    balance_amount: totaliteamt,
    company_id: companyid,
    vendor_id: vendorid,
    customer_id: 1,
    created_by: 1,
    updated_by: 1,
    trans_params: itemdetailsarray,
    tax_params: alltaxarrays,
    transaction_date: formatDate(new Date())
  };

  logindata = {
    params: data
  };
  $('itemdtl').addClass('hide');
  $('itemdtl').removeClass('hide');
  api.post('purchasetransaction/', logindata, function (data) {
    localStorage.setItem('itemjsondata', data);


    $(".login").addClass("hide");
    $(".vendor-details").addClass("hide");
    $(".profile-update").addClass("hide");
    $(".purchase-payments").addClass("hide");
    $(".change-password").addClass("hide");
    $(".purchaselist").removeClass("hide");
    $(".payments-list").addClass("hide");
    $(".purchase-payments").addClass("hide");
    var d = new Date();
    var month = d.getMonth() + 1;
    var lastmonth = d.getMonth();
    var day = d.getDate();
    var today = d.getFullYear() + '-' +
      (('' + month).length < 2 ? '0' : '') + month + '-' +
      (('' + day).length < 2 ? '0' : '') + day;

    var lastmonthdate = d.getFullYear() + '-' +
      (('' + lastmonth).length < 2 ? '0' : '') + lastmonth + '-' +
      (('' + day).length < 2 ? '0' : '') + day;
    $('#dtpurchasetodate').val(today);
    $('#dtpurchasefromdate').val(lastmonthdate);
    appdata.getpurvendor(); //get vendor details
    appdata.getcompanys();
    var data123 = purvendorid;
    appdata.getpurchaselist(today, lastmonthdate, data123);
  });
};


//display selected item details
appdata.viewinsertitemdtls = function (itemdetailsarray, alltaxarrays, gstarray, sgstarray, cgstarray, measurementarray,
  pricearray, runningstockarray, qtyarray, itemtotalpricearray, itemidarray, gsttypeidarray,
  sgsttypeidarray, cgsttypeidarray, gstcalarray, sgstcalarray, cgstcalarray) {
  $('.itemdtl').addClass('hide');
  $('.viewitemdtl').removeClass('hide');
  $('.submititems').addClass('hide');
  $('.payitems').removeClass('hide');
  $('.paytobck').addClass('hide');


  var daaa1 = '';
  for (var i = 0; i <= gstarray.length - 1; i++) {
    var unit_of_measurement = measurementarray[i];
    var price = pricearray[i];
    var available_quantity = runningstockarray[i];
    var item_code = itemidarray[i];
    var item_id = itemidarray[i];
    var item_name = 1;
    var tax = 1;
    var SGST = sgstarray[i];
    var CGST = cgstarray[i];
    var GST = gstarray[i];
    var gsttypeid = 0;
    var sgsttypeid = 0;
    var cgsttypeid = 0;
    var total = price * qtyarray[i];
    var gstcal = total * GST / 100;
    var sgstcal = total * SGST / 100;
    var cgstcal = total * CGST / 100;
    var totaltaxamt = + sgstcal + cgstcal;
    var tot = total + totaltaxamt;
    var itemname = itemnames[i];
    var totamt = itemtotalpricearray[i];



    daaa1 += '<div class="itemslistview">';

    daaa1 += '<div class="row">';
    daaa1 += '<div class="col-sm-12">';
    daaa1 += '<div class="row">';
    daaa1 += '<div class="col-sm-6">';
    daaa1 += '<p>Product Name </p>';
    daaa1 += '</div>';
    daaa1 += '<div class="col-sm-6">';
    daaa1 += '<p>:<span>' + itemname + '</span></p>';
    daaa1 += '</div>';
    daaa1 += '</div>';
    daaa1 += '</div>';
    daaa1 += '</div>';

    daaa1 += '<div class="row">';
    daaa1 += '<div class="col-sm-12">';
    daaa1 += '<div class="row">';
    daaa1 += '<div class="col-sm-6">';
    daaa1 += '<p>Quantity  </p>';
    daaa1 += '</div>';
    daaa1 += '<div class="col-sm-6">';
    daaa1 += '<p>:<span>' + qtyarray[i] + '</span></p>';
    daaa1 += '</div>';
    daaa1 += '</div>';
    daaa1 += '</div>';
    daaa1 += '</div>';

    daaa1 += '<div class="row">';
    daaa1 += '<div class="col-sm-12">';
    daaa1 += '<div class="row">';
    daaa1 += '<div class="col-sm-6">';
    daaa1 += '<p>Measurement </p>';
    daaa1 += '</div>';
    daaa1 += '<div class="col-sm-6">';
    daaa1 += '<p>:<span>' + unit_of_measurement + '</span></p>';
    daaa1 += '</div>';
    daaa1 += '</div>';
    daaa1 += '</div>';
    daaa1 += '</div>';


    daaa1 += '<div class="row">';
    daaa1 += '<div class="col-sm-12">';
    daaa1 += '<div class="row">';
    daaa1 += '<div class="col-sm-6">';
    daaa1 += '<p>Price :</p>';
    daaa1 += '</div>';
    daaa1 += '<div class="col-sm-6">';
    daaa1 += '<p>:<span>' + price + '</span></p>';
    daaa1 += '</div>';
    daaa1 += '</div>';
    daaa1 += '</div>';
    daaa1 += '</div>';

    daaa1 += '<div class="row">';
    daaa1 += '<div class="col-sm-12">';
    daaa1 += '<div class="row">';
    daaa1 += '<div class="col-sm-6">';
    daaa1 += '<p>Total Amount </p>';
    daaa1 += '</div>';
    daaa1 += '<div class="col-sm-6">';
    daaa1 += '<p>:<span>' + totamt + '</span></p>';
    daaa1 += '</div>';
    daaa1 += '</div>';
    daaa1 += '</div>';
    daaa1 += '</div>';


    daaa1 += '<div class="itemlistshowdata hide">';
    daaa1 += '<div class="row">';
    daaa1 += '<div class="col-sm-4">';
    daaa1 += '<p>IGST</p>';
    daaa1 += '<p class="gstdata gst-' + 1 + '" data-tes=' + GST + '>' + GST + '%' + '</p>';
    daaa1 += '<p>' + gstcal + '</p>';
    daaa1 += '</div>';
    daaa1 += '<div class="col-sm-4">';
    daaa1 += '<p>CGST</p>';
    daaa1 += '<p class="cgstdata cgst-' + 1 + '" data-tes=' + CGST + '>' + CGST + '%' + '</p>';
    daaa1 += '<p>' + cgstcal + '</p>';
    daaa1 += '</div>';
    daaa1 += '<div class="col-sm-4">';
    daaa1 += '<p>SGST</p>';
    daaa1 += '<p class="sgstdata sgst-' + 1 + '" data-tes=' + SGST + '>' + SGST + '%' + '</p>';
    daaa1 += '<p >' + sgstcal + '</p>';
    daaa1 += '</div>';
    daaa1 += '</div>';
    daaa1 += '</div>';





    daaa1 += '<div class="row">';
    daaa1 += '<div class="col-sm-12">';
    daaa1 += '<div class="next-btn showlist" style="margin-left:15px;" onclick="showitellistgstdata()">';
    daaa1 += '<a>More</a>';
    daaa1 += '</div>';
    daaa1 += '<div class="next-btn hidelist hide" style="margin-left:15px;" onclick="hideitellistgstdata()">';
    daaa1 += '<a>Hide</a>';
    daaa1 += '</div>';
    daaa1 += '</div>';
    daaa1 += '</div>';
    daaa1 += '</div>';
    daaa1 += '<br/>';
  }
  $('.viewdata').append(daaa1);
};


//show purchase list items
appdata.getpurchaselist = function (currentmonthdate, lastmonthdate, purvendorid) {

  var company_id = localStorage.getItem('company_id');
  purvendorid = null;

  var purchaselistobj = {
    "company_id": company_id,
    "from_date": lastmonthdate,
    "to_date": currentmonthdate,
    "vendor_id": purvendorid
  };
  api.post('getpurchaselist/', purchaselistobj, function (data) {
    var itemdetails = $.parseJSON(data);
    $(".itemslistview").addClass("hide");
    if (itemdetails.length != 0) {
      var daaa1 = "";
      console.log(itemdetails);
      for (i = 0; i <= itemdetails.length - 1; i++) {
        daaa1 += '<div class="purinnertable">';
        daaa1 += '<div class="row">';
        daaa1 += '<div class="col-sm-12">';
        daaa1 += '<div class="row">';
        daaa1 += '<div class="col-sm-6">';
        daaa1 += '<p>Invoice No </p>';
        daaa1 += '</div>';
        daaa1 += '<div class="col-sm-6">';
        daaa1 += '<p>:<span>' + itemdetails[i]['transaction_no'] + '</span></p>';
        daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '<div class="row">';
        daaa1 += '<div class="col-sm-12">';
        daaa1 += '<div class="row">';
        daaa1 += '<div class="col-sm-6">';
        daaa1 += '<p>Vendor  </p>';
        daaa1 += '</div>';
        daaa1 += '<div class="col-sm-6">';
        daaa1 += '<p>:<span>' + itemdetails[i]['vendor_name'] + '</span></p>';
        daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '<div class="row">';
        daaa1 += '<div class="col-sm-12">';
        daaa1 += '<div class="row">';
        daaa1 += '<div class="col-sm-6">';
        daaa1 += '<p>Date </p>';
        daaa1 += '</div>';
        daaa1 += '<div class="col-sm-6">';
        daaa1 += '<p>:<span>' + itemdetails[i]['transaction_date'] + '</span></p>';
        daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '<div class="row">';
        daaa1 += '<div class="col-sm-12">';
        daaa1 += '<div class="row">';
        daaa1 += '<div class="col-sm-6">';
        daaa1 += '<p>Total Amount </p>';
        daaa1 += '</div>';
        daaa1 += '<div class="col-sm-6">';
        daaa1 += '<p>:<span>' + itemdetails[i]['total_amount'] + '</span></p>';
        daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '<div class="row">';
        daaa1 += '<div class="col-sm-12">';
        daaa1 += '<div class="row">';
        daaa1 += '<div class="col-sm-6">';
        daaa1 += '<p>Balance Amount </p>';
        daaa1 += '</div>';
        daaa1 += '<div class="col-sm-6">';
        daaa1 += '<p>:<span>' + itemdetails[i]['balance_amount'] + '</span></p>';
        daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '<div class="row">';
        daaa1 += '<div class="col-sm-12">';
        daaa1 += '<div class="row">';
        daaa1 += '<div class="col-sm-6">';
        daaa1 += '<p>Status :</p>';
        daaa1 += '</div>';
        daaa1 += '<div class="col-sm-6">';
        daaa1 += '<p><span>' + itemdetails[i]['status'] + '</span></p>';
        daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '</div>';

        daaa1 += '<div class="row">';
        daaa1 += '<div class="col-sm-12">';
        daaa1 += '<div class="next-btn" style="margin-left:15px;" id="showitemdtls" onclick="paymentitem(' + itemdetails[i]['transaction_id'] + ',' + itemdetails[i]['balance_amount'] + ')">';
        daaa1 += '<a>Pay</a>';
        daaa1 += '</div>';
        // daaa1 += '<div class="next-btn" id="showitemdtls" onclick="editpurchasesitems(' + itemdetails[i]['transaction_id'] + ')">';
        // daaa1 += '<a>Edit</a>';
        // daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '</div>';
        daaa1 += '<br/>';
      }
      $('.purchaseitemslist').append(daaa1);
      $('.loading').removeClass('spinner');
    } else {
      appdata.getvendor();
      $(".purchaselist").addClass("hide");
      $(".vendor-details").removeClass("hide");
      $(".showvendordtls").removeClass("hide");

      $(".itemslistview").addClass("hide");
      $(".viewitemdtl").addClass("hide");
      $(".payitems").addClass("hide");
      $("#vendor").val("");
      $("#mobileno").val("");
      $("#address").val("");
      $("#panno").val("");
      $("#aadharno").val("");
      $('.loading').removeClass('spinner');
    }
  });

};
//edit purchase list
appdata.editpurchasesitems = function (id) {
  var editpurchasesitems = {
    "transaction_id": id
  };
  api.post('getpurchasedetails/', editpurchasesitems, function (data) {
    var itemdetails = $.parseJSON(data);
    var daaa1 = "";
    console.log(itemdetails);
    var puritemlist = itemdetails[0]['transaction_item'];
    var itemhtml = "";

    for (i = 0; i <= puritemlist.length - 1; i++) {
      var unit_of_measurement = 1;
      var available_quantity = 1;
      var sgst = 0;
      var cgst = 0;
      var gst = 0;
      var gsttypeid = 0;
      var sgsttypeid = 0;
      var cgsttypeid = 0;
      var total = puritemlist[i]['amount'] * puritemlist[i]['quantity'];

      if (puritemlist[i]['item_taxes']['tax_amount1'] != "") {
        sgst = puritemlist[i]['item_taxes']['tax_amount1'];
      }
      if (puritemlist[i]['item_taxes']['tax_amount2'] != "") {
        cgst = puritemlist[i]['item_taxes']['tax_amount2'];
      }
      if (puritemlist[i]['item_taxes']['tax_amount3'] != "") {
        gst = puritemlist[i]['item_taxes']['tax_amount3'];
      }
      var totaltaxamt = parseFloat(sgst) + parseFloat(cgst) + parseFloat(gst);
      var totalamt = total + totaltaxamt;











      itemhtml += '<div class="itemcontent" style="margin-top: 10px;"><div class="card innercard"><div></div><div class="row"><div class="col-sm-5"><p class="justcolor">Product Name:</p></div><div class="col-sm-7"><input id="disitem" value=' + puritemlist[i]['item_name'] + '   type="text" dir="123" class="searchitem searchInput disitem" disable/><p class="empty-message"></p></div></div><br/><div class="itemdetails">';




      itemhtml += '<div class="row">';
      itemhtml += '<div class="col-md-6">';
      itemhtml += '<p class="hide"><span class="itemid" data-tes=' + puritemlist[i]['item_id'] + '>' + puritemlist[i]['item_id'] + '</span></p>';
      itemhtml += '<p class="hide"><span class="totaltaxamt gst-' + totaltaxamt + '" data-tes=' + totaltaxamt + '>' + totaltaxamt + '</span></p>';

      //tax types ids
      itemhtml += '<p class="hide"><span class="gsttypeid" data-tes=' + gsttypeid + '>' + gsttypeid + '</span></p>';
      itemhtml += '<p class="hide"><span class="sgsttypeid" data-tes=' + sgsttypeid + '>' + sgsttypeid + '</span></p>';
      itemhtml += '<p class="hide"><span class="cgsttypeid" data-tes=' + cgsttypeid + '>' + cgsttypeid + '</span></p>';
      //each tax calculatation amt
      itemhtml += '<p class="hide"><span class="gstcal" data-tes=' + puritemlist[i]['item_taxes']['tax_amount3'] + '>' + puritemlist[i]['item_taxes']['tax_amount3'] + '</span></p>';
      itemhtml += '<p class="hide"><span class="sgstcal" data-tes=' + puritemlist[i]['item_taxes']['tax_amount2'] + '>' + puritemlist[i]['item_taxes']['tax_amount2'] + '</span></p>';
      itemhtml += '<p class="hide"><span class="cgstcal" data-tes=' + puritemlist[i]['item_taxes']['tax_amount1'] + '>' + puritemlist[i]['item_taxes']['tax_amount1'] + '</span></p>';




      itemhtml += '<p class="justcolor">Quantity</p>';
      itemhtml += '<div class="plusminus">';
      itemhtml += '<div class="value-button valminus" id="decrease" onclick="decreaseValue(' + puritemlist[i]['item_id'] + ')" value="Decrease Value">-</div>';
      itemhtml += '<input type="number" id="number-' + puritemlist[i]['item_id'] + '" value=' + puritemlist[i]['quantity'] + ' class="qty displaycount" />';
      itemhtml += '<div class="value-button valplus" id="increase" onclick="increaseValue(' + puritemlist[i]['item_id'] + ')" value="Increase Value">+</div>';
      itemhtml += '<div class="clear"></div>';
      itemhtml += '</div>';

      itemhtml += '</div>';
      itemhtml += '<div class="col-md-6">';
      itemhtml += '<p class="justcolor">Measurement  <span class="datameasure measurement-' + puritemlist[i]['item_id'] + '" data-tes=' + unit_of_measurement + '>' + unit_of_measurement + '</span></p>';
      itemhtml += '<p class="measuresty">Price<span class="itemprice datameasure1 price-' + puritemlist[i]['item_id'] + '" data-tes=' + puritemlist[i]['amount'] + '>' + puritemlist[i]['amount'] + '</span></p>';
      itemhtml += '<p class="measuresty">Running Stock<span class="itemstock datameasure1 runqty-' + puritemlist[i]['item_id'] + '" data-tes=' + available_quantity + '>' + available_quantity + '</span></p>';
      itemhtml += '</div>';
      itemhtml += '</div>';

      itemhtml += '<div class="alltaxs hide">';
      itemhtml += '<div class="row">';
      itemhtml += '<div class="col-sm-4">';
      itemhtml += '<p>GST : <span class="gstdata gst-' + puritemlist[i]['item_id'] + '" data-tes=' + puritemlist[i]['item_taxes']['tax_percentage3'] + '>' + puritemlist[i]['item_taxes']['tax_percentage3'] + '</span></p>';
      itemhtml += '</div>';
      itemhtml += '<div class="col-sm-4">';
      itemhtml += '<p>CGST : <span class="cgstdata cgst-' + puritemlist[i]['item_id'] + '" data-tes=' + puritemlist[i]['item_taxes']['tax_percentage2'] + '>' + puritemlist[i]['item_taxes']['tax_percentage2'] + '</span></p>';
      itemhtml += '</div>';
      itemhtml += '<div class="col-sm-4">';
      itemhtml += '<p>SGST : <span class="sgstdata sgst-' + puritemlist[i]['item_id'] + '" data-tes=' + puritemlist[i]['item_taxes']['tax_percentage1'] + '>' + puritemlist[i]['item_taxes']['tax_percentage1'] + '</span></p>';
      itemhtml += '</div>';
      itemhtml += '</div>';
      itemhtml += '</div>';
      itemhtml += '</div>';
      itemhtml += '<div class="row">';
      itemhtml += '<div class="col-md-6">';
      itemhtml += '<p class="justcolor">Total:<span class="itemtotalprice boldcls grandtotal' + puritemlist[i]['item_id'] + '" data-tes=' + totalamt + '>' + totalamt + '</span></p>';
      itemhtml += '</div>';
      itemhtml += ' <div class="view-more firstrecirdshow" style="margin-left:15px;" onclick="showfirstrecord(' + puritemlist[i]['item_id'] + ')">';
      itemhtml += '<a>more</a>';
      itemhtml += '</div>';
      itemhtml += ' <div class="view-more firstrecirdhide hide" style="margin-left:15px;" onclick="hidefirstrecord(' + puritemlist[i]['item_id'] + ')">';
      itemhtml += '<a>Hide</a>';
      itemhtml += '</div>';
      itemhtml += ' <div class="view-more" style="margin-left:15px;" onclick="deleterecord(' + puritemlist[i]['item_id'] + ')">';
      itemhtml += '<a>Delete</a>';
      itemhtml += '</div>';
      itemhtml += '</div>';
      itemhtml += '</div></div></div>';
    }
    $('.purdtl').append(itemhtml);
  });
}


// change password page//
appdata.changepassword = function () {
  var uid = localStorage.getItem("user_id");
  var pwdValid = "";
  pwdValid = "Password should contain:\n ";
  pwdValid += "1. Minimum 8 characters \n";
  pwdValid += "2. Atleast one small and one capital letter \n";
  pwdValid += "3. Atleast one special character \n";
  pwdValid += "4. Atleast one digit \n 5. No spaces";

  if ($("#txtOldPwd").val() == "") {
    mes = "Please enter old password";
    appdata.warningmsg(mes);
    $("#txtOldPwd").focus();
    return;
  }

  if ($("#txtNewPwd").val() == "") {
    mes = "Please enter new password";
    appdata.warningmsg(mes);
    $("#txtNewPwd").focus();
    return;
  }

  if ($("#txtCnfNewPwd").val() == "") {
    mes = "Please enter confirm password";
    appdata.warningmsg(mes);
    $("#txtCnfNewPwd").focus();
    return;
  }

  if ($("#txtNewPwd").val() != $("#txtCnfNewPwd").val()) {
    mes = "New password and Confirm password mismatched";
    appdata.warningmsg(mes);
    return;
  }

  if (
    $("#txtOldPwd").val().length > 8 &&
    $("#txtOldPwd")
      .val()
      .match(/([a-zA-Z])/) &&
    $("#txtOldPwd")
      .val()
      .match(/([0-9])/) &&
    $("#txtOldPwd")
      .val()
      .match(/([^\w\s])/) &&
    !$("#txtOldPwd")
      .val()
      .match(/([\s])/)
  ) { } else {
    mes = "Old password requirement is not matched \n" + pwdValid;
    appdata.warningmsg_pwdchng(mes);
    $("#txtOldPwd").focus();
    return;
  }

  if (
    $("#txtNewPwd").val().length > 8 &&
    $("#txtNewPwd")
      .val()
      .match(/([a-zA-Z])/) &&
    $("#txtNewPwd")
      .val()
      .match(/([0-9])/) &&
    $("#txtNewPwd")
      .val()
      .match(/([^\w\s])/) &&
    !$("#txtNewPwd")
      .val()
      .match(/([\s])/)
  ) { } else {
    mes = "New password requirement is not matched \n " + pwdValid;
    appdata.warningmsg_pwdchng(mes);
    $("#txtNewPwd").focus();
    return;
  }

  if (
    $("#txtCnfNewPwd").val().length > 8 &&
    $("#txtCnfNewPwd")
      .val()
      .match(/([a-zA-Z])/) &&
    $("#txtCnfNewPwd")
      .val()
      .match(/([0-9])/) &&
    $("#txtCnfNewPwd")
      .val()
      .match(/([^\w\s])/) &&
    !$("#txtCnfNewPwd")
      .val()
      .match(/([\s])/)
  ) {
    var userDetails = {
      user_id: localStorage.getItem("user_id"),
      password: $("#txtNewPwd").val(),
      oldpassword: $("#txtOldPwd").val()
    };

    userDetails_params = {
      params: userDetails
    };
    $(".loading").addClass("spinner");
    // api to get password and update password by Id
    api.post1("changepwdbyid/", userDetails, function (data) {
      $(".loading").removeClass("spinner");
      mes = data;
      appdata.successmsg(mes);
    });

  } else {
    mes = "Confirm password requirement is not matched \n" + pwdValid;
    appdata.warningmsg_pwdchng(mes);
    $("#txtNewPwd").focus();
    return;
  }
};

// profile update page//
appdata.profileupdate = function () {
  $('.loading').addClass('spinner');
  var phNoPattern = /([0-9]{10})|(\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4})/;
  var emailPattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
  var pincodePattern = /^\d{6}$/;

  var userDetails = {
    user_id: localStorage.getItem("user_id")
  };

  getUserData_params = {
    params: userDetails
  };
  api.post1("getuserdetails/", getUserData_params, function (data) {
    user_details = data;
    $('.loading').removeClass('spinner');

    $("#txtFName").val(user_details[0]["first_name"]);
    $("#txtLName").val(user_details[0]["last_name"]);
    $("#txtPhno").val(user_details[0]["phone"]);
    $("#txtEmail").val(user_details[0]["email"]);
    $("#txtAddress1").val(user_details[0]["address1"]);
    $("#txtAddress2").val(user_details[0]["address2"]);
    $("#txtDistrict").val(user_details[0]["district"]);
    $("#txtCity").val(user_details[0]["city"]);
    $("#txtPincode").val(user_details[0]["pincode"]);

    var conuntryID = user_details[0]["country_id"];
    var stateID = user_details[0]["state_id"];

    if (conuntryID != 0) {
      $("#ddCountry").empty();
      api.post1("getcountries/", "", function (data) {
        countries = data;
        $("#ddCountry").append(
          '<option value="0"><--Select Country--></option>'
        );

        $.each(countries, function (index, values) {
          $("#ddCountry").append(
            '<option value="' +
            values.country_id +
            '">' +
            values.country_name +
            "</option>"
          );
          if (values.country_id == conuntryID) {
            $("#ddCountry").val(conuntryID);
            cid = {
              country_id: conuntryID
            };
            cid_params = {
              params: cid
            };

            $("#ddState").empty();
            api.post1("getstates/", cid_params, function (data1) {
              $("#ddState").append(
                '<option value="0"><--Select State--></option>'
              );
              states = data1;
              if (stateID != 0) {
                $.each(states, function (index, values1) {
                  $("#ddState").append(
                    '<option value="' +
                    values1.state_id +
                    '">' +
                    values1.state_name +
                    "</option>"
                  );
                  if (values1.state_id == stateID) {
                    $("#ddState").val(stateID);
                  }
                });
              }
            });
          }
        });
      });
    }

    $("#ddCountry").change(function () {
      var val = $("#ddCountry").val();
      cid = {
        country_id: val
      };
      cid_params1 = {
        params: cid
      };
      $("#ddState").empty();
      if (val != 0) {
        api.post1("getstates/", cid_params1, function (data1) {
          $("#ddState").append('<option value="0"><--Select State--></option>');
          states = data1;
          if (states.length != 0) {
            $.each(states, function (index, values1) {
              $("#ddState").append(
                '<option value="' +
                values1.state_id +
                '">' +
                values1.state_name +
                "</option>"
              );
            });
          }
        });
      } else {
        $("#ddState").append('<option value="0"><--Select State--></option>');
      }
    });
  });

  $("#btnProfUpd").on("click", function () {
    // fields validation //
    if ($("#txtFName").val() == "") {
      mes = "Please enter first name";
      appdata.warningmsg(mes);
      $("#txtFName").focus();
      return;
    }

    if ($("#txtLName").val() == "") {
      mes = "Please enter last name";
      appdata.warningmsg(mes);
      $("#txtLName").focus();
      return;
    }

    if ($("#txtPhno").val() == "") {
      mes = "Please enter phone number";
      appdata.warningmsg(mes);
      $("#txtPhno").focus();
      return;
    } else {
      if (
        $("#txtPhno")
          .val()
          .match(phNoPattern)
      ) { } else {
        mes = "Please enter valid phone number";
        appdata.warningmsg(mes);
        $("#txtPhno").focus();
        return;
      }
    }

    if ($("#txtEmail").val() == "") {
      mes = "Please enter email";
      appdata.warningmsg(mes);
      $("#txtEmail").focus();
      return;
    } else {
      if (
        $("#txtEmail")
          .val()
          .match(emailPattern)
      ) { } else {
        mes = "Please enter valid email";
        appdata.warningmsg(mes);
        $("#txtEmail").focus();
        return;
      }
    }

    if ($("#txtAddress1").val() == "") {
      mes = "Please enter address1";
      appdata.warningmsg(mes);
      $("#txtAddress1").focus();
      return;
    }

    if ($("#ddCountry").val() == 0) {
      mes = "Please select country";
      appdata.warningmsg(mes);
      $("#ddCountry").focus();
      return;
    }

    if ($("#ddState").val() == 0) {
      mes = "Please select state";
      appdata.warningmsg(mes);
      $("#ddState").focus();
      return;
    }

    if ($("#txtDistrict").val() == "") {
      mes = "Please enter district";
      appdata.warningmsg(mes);
      $("#txtDistrict").focus();
      return;
    }

    if ($("#txtCity").val() == "") {
      mes = "Please enter city";
      appdata.warningmsg(mes);
      $("#txtCity").focus();
      return;
    }

    if ($("#txtPincode").val() == "") {
      mes = "Please enter pincode";
      appdata.warningmsg(mes);
      $("#txtPincode").focus();
      return;
    } else {
      if (
        $("#txtPincode")
          .val()
          .match(pincodePattern)
      ) { } else {
        mes = "Please enter valid pincode";
        appdata.warningmsg(mes);
        $("#txtPincode").focus();
        return;
      }
    }

    // fields validation //

    var userDetails1 = {
      user_id: localStorage.getItem("user_id"),
      first_name: $("#txtFName").val(),
      last_name: $("#txtLName").val(),
      phone: $("#txtPhno").val(),
      email: $("#txtEmail").val(),
      address1: $("#txtAddress1").val(),
      address2: $("#txtAddress2").val(),
      country_id: $("#ddCountry").val(),
      state_id: $("#ddState").val(),
      district: $("#txtDistrict").val(),
      city: $("#txtCity").val(),
      pincode: $("#txtPincode").val(),
      updated_by: localStorage.getItem("user_id")
    };

    userDetails1_profupd = {
      params: userDetails1
    };
    $('.loading').addClass('spinner');
    // update profile //
    api.post1("upduserprofile/", userDetails1_profupd, function (data) {
      $('.loading').removeClass('spinner');
      mes = 'Profile is ' + data;
      appdata.successmsg(mes);

    });
  });
};

// purchase payments
appdata.purchasepayments = function (invoiceid, balAmount) {

  // $("#divPurPayOnline").hide();
  $("#divPurPayCheque").hide();

  var invoiceid = localStorage.getItem('invoiceid');

  emptyStr = "";
  c_id = localStorage.getItem("company_id")
  compID = {
    company_id: localStorage.getItem("company_id")
  }


  if ($(".loading").addClass("spinner")) {
    $(".loading").removeClass("spinner")
  }

  //clear fields data
  function empFields() {
    $("#ddPaymentType").val(0);
    $("#txtPaymentAmount").val("");
    $("#ddBankAccId").val(0);
    $("#txtbankAccNo").val("");
    $("#txtRefNo").val("");
    $("#txtChequeNo").val("");
    $("#txtBankName").val("");
    $("#txtBranchName").val("");
    $("#txtIFSCCode").val("");
    $("#txtBankAccountNoCheque").val("");
    $("#txtBankAccountName").val("");
    $("#txtDescription").val("");
  }

  $("#ddBankAccId").empty();
  $("#ddBankAccId").append(
    '<option value="0">Select Account Name</option>'
  );
  var compid = localStorage.getItem("company_id")
  var compID = {
    company_id: compid
  }
  api.post1("getbankaccountnames/", compID, function (data1) {
    accNames = data1;
    if (accNames.length > 0) {
      $.each(accNames, function (index, values1) {
        $("#ddBankAccId").append(
          '<option value="' +
          values1.bank_account_id +
          '">' +
          values1.account_name +
          "</option>"
        );
      });
    }
  });

  $("#ddBankAccId").change(function () {
    if ($("#ddBankAccId").val() != 0) {
      bank_acc_id = $("#ddBankAccId").val();
      accID = {
        bank_account_id: bank_acc_id
      }
      api.post1("getbankdetailsforpayment/", accID, function (data) {
        if (data.length > 0) {
          bank_details = data;
          $("#txtBankName").val(bank_details[0]['bank_name']);
          $("#txtBranchName").val(bank_details[0]['branch_name'])
          $("#txtIFSCCode").val(bank_details[0]['ifsc'])
          $("#txtBankAccountNoCheque").val(bank_details[0]['bank_account_no'])
          $("#txtBankAccountName").val(bank_details[0]['bank_account_name'])
        }
      });
    }
  })


  function formatDate1(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  $(function () {
    $("#dtpPaymentDate").datepicker();
    var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth() + 1; //Months are zero based
    var curr_year = d.getFullYear();
    today = curr_year + "-" + curr_month + "-" + curr_date;
    // var today = new Date().getFullYear()+'-'+("0"+(new Date().getMonth()+1)).slice(-2)+'-'+("0"+new Date().getDate()).slice(-2)
    $("#dtpPaymentDate").val(formatDate1(today));
    $("#dtpPaymentDate").on("change", function () {
      $("#dtpPaymentDate").val(formatDate1($("#dtpPaymentDate").val()));
    });
  });



  $("#ddPaymentType").change(function () {
    if ($("#ddPaymentType").val() == 1) {
      // $("#divPurPayOnline").hide();
      $("#divPurPayCheque").hide();
    }

    if ($("#ddPaymentType").val() == 2) {
      // $("#divPurPayOnline").show();
      $("#divPurPayCheque").show();
    }

    // if ($("#ddPaymentType").val() == 3) {
    //   // $("#divPurPayOnline").hide();
    //   $("#divPurPayCheque").show();
    // }
  });



  $("#btnPaymentConfirm").off('click').on("click", function () {

    getPayListDetails();
  });

  function getPayListDetails() {
    if ($("#ddPaymentType").val() == 0) {
      mes = "Please select payment type";
      appdata.warningmsg(mes);
      $("#ddPaymentType").focus();
      return;
    }

    if ($("#dtpPaymentDate").val() == "") {
      mes = "Please select payment date";
      appdata.warningmsg(mes);
      $("#dtpPaymentDate").focus();
      return;
    }

    if ($("#txtPaymentAmount").val() == "") {
      mes = "Please enter amount";
      appdata.warningmsg(mes);
      $("#txtPaymentAmount").focus();
      return;
    }

    if ($("#ddBankAccId").val() == 0) {
      mes = "Please select bank account name";
      appdata.warningmsg(mes);
      $("#ddBankAccId").focus();
      return;
    }



    // insert cash payment
    if ($("#ddPaymentType").val() == 1) {



      if (balAmount < $("#txtPaymentAmount").val()) {
        mes = "Insuffient balance amount";
        appdata.warningmsg(mes);
        return;
      }


      var payment_Cash = {
        // transaction: [{
        //   transaction_id: invoiceid,
        //   payment_amount: $("#txtPaymentAmount").val()
        // }],
        transaction: [{
          transaction_id: invoiceid,
          payment_amount: $("#txtPaymentAmount").val()
        }],
        account_head_id: 2,
        transaction_type: "Dr",
        payment_type: $("#ddPaymentType option:selected").text(),
        bank_account_id: $("#ddBankAccId").val(),
        cheque_no: emptyStr,
        bank_name: emptyStr,
        branch_name: emptyStr,
        ifsc: emptyStr,
        bank_account_no: emptyStr,
        bank_account_name: emptyStr,
        deposit_date: $("#dtpPaymentDate").val(),
        reference_no: emptyStr,
        payment_amount: $("#txtPaymentAmount").val(),
        payment_date: $("#dtpPaymentDate").val(),
        status: "Closed",
        created_by: localStorage.getItem("user_id"),
        updated_by: localStorage.getItem("user_id"),
        company_id: c_id,
        description: $("#txtDescription").val()
      };
      $(".loading").addClass("spinner");
      api.post1("insertpayment/", payment_Cash, function (data) {
        $(".loading").removeClass("spinner");
        mes = data;
        appdata.successmsg('Payment is ' + mes['result']);
        empFields();
        $('.purchase-payments').addClass('hide');
        appdata.paymentslist(invoiceid);
      });

      payment_Cash.length = 0;

    }

    // cheque payment fields validation
    if ($("#divPurPayCheque").is(":visible") == true) {


      if ($("#txtChequeNo").val() == "") {
        mes = "Please enter cheque no";
        appdata.warningmsg(mes);
        $("#txtChequeNo").focus();
        return;
      }

      if ($("#txtBankName").val() == "") {
        mes = "Please enter bank name";
        appdata.warningmsg(mes);
        $("#txtBankName").focus();
        return;
      }

      if ($("#txtBranchName").val() == "") {
        mes = "Please enter branch name";
        appdata.warningmsg(mes);
        $("#txtBranchName").focus();
        return;
      }

      if ($("#txtIFSCCode").val() == "") {
        mes = "Please enter IFSC code";
        appdata.warningmsg(mes);
        $("#txtIFSCCode").focus();
        return;
      }

      if ($("#txtBankAccountNoCheque").val() == "") {

        mes = "Please enter bank account no";
        appdata.warningmsg(mes);
        $("#txtBankAccountNoCheque").focus();
        return;
      }

      if ($("#txtBankAccountName").val() == "") {
        mes = "Please enter bank name";
        appdata.warningmsg(mes);
        $("#txtBankAccountName").focus();
        return;
      }
    }

    // insert Cheque payment
    if ($("#ddPaymentType").val() == 3) {

      if (balAmount < $("#txtPaymentAmount").val()) {
        mes = "Insuffient balance amount";
        appdata.warningmsg(mes);
        return;
      }


      var payment_Cheque = {
        transaction: [{
          transaction_id: invoiceid,
          payment_amount: $("#txtPaymentAmount").val()
        }],
        account_head_id: 2,
        transaction_type: "Dr",
        payment_type: $("#ddPaymentType option:selected").text(),
        bank_account_id: $("#ddBankAccId").val(),
        cheque_no: $("#txtChequeNo").val(),
        bank_name: $("#txtBankName").val(),
        branch_name: $("#txtBranchName").val(),
        ifsc: $("#txtIFSCCode").val(),
        bank_account_no: $("#txtBankAccountNoCheque").val(),
        bank_account_name: $("#txtBankAccountName").val(),
        deposit_date: $("#dtpPaymentDate").val(),
        reference_no: null,
        payment_amount: $("#txtPaymentAmount").val(),
        payment_date: $("#dtpPaymentDate").val(),
        status: "Open",
        created_by: localStorage.getItem("user_id"),
        updated_by: localStorage.getItem("user_id"),
        company_id: c_id,
        description: $("#txtDescription").val()
      };
      $(".loading").addClass("spinner");
      api.post1("insertpayment/", payment_Cheque, function (data) {
        $(".loading").removeClass("spinner");
        mes = data;
        appdata.successmsg('Payment is ' + mes['result']);
        empFields();
        $('.purchase-payments').addClass('hide');
        appdata.paymentslist(invoiceid);
      });

      payment_Cheque = [];

    }
  }
};

appdata.paymentslist = function (invoiceid, balAmount) {
  var invoice_id = invoiceid;
  trans_id = {
    transaction_id: invoice_id
  }

  api.post1('gettransactionpayment/', trans_id, function (data) {
    if (data.length > 0) {
      $('.payments-list').removeClass('hide');
      // $(".loading").addClass("spinner");
      appdata.showpaymentslist(data, invoiceid);
      $(".loading").removeClass("spinner");

    } else {
      $('.purchase-payments').removeClass('hide');
      appdata.purchasepayments(invoiceid, balAmount);
      $('.payments-list').addClass('hide');
      $(".loading").removeClass("spinner");

    }
  })
}

appdata.showpaymentslist = function (paymentsList, invoiceid) {

  $('#divPaymentsList').empty();
  var daaa1 = "";



  for (i = 0; i <= paymentsList.length - 1; i++) {

    if (paymentsList[i]['payment_type'] == "Cash") {


      var transaction_id = invoiceid;
      var payment_id = paymentsList[i]['payment_id'];
      var payment_amt = paymentsList[i]['payment_amount'];




      daaa1 += '<div class="purinnertable">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p>Payment Type :</p>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p><span>' + paymentsList[i]['payment_type'] + '</span></p>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';

      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p>Payment Date :</p>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p><span>' + paymentsList[i]['payment_date'] + '</span></p>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';

      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p>Payment Amount :</p>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p><span>' + paymentsList[i]['payment_amount'] + '</span></p>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';

      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<div class="next-btn" id="delPayment" onclick="delPayments(' + transaction_id + ',' + payment_id + ',' + payment_amt + ');">';
      daaa1 += '<a>Delete</a>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '<br/>';

    }

    if (paymentsList[i]['payment_type'] == "Online") {

      var transaction_id = invoiceid;
      var payment_id = paymentsList[i]['payment_id'];
      var payment_amt = paymentsList[i]['payment_amount'];


      daaa1 += '<div class="purinnertable">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p>Payment Type :</p>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p><span>' + paymentsList[i]['payment_type'] + '</span></p>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';

      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p>Payment Date :</p>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p><span>' + paymentsList[i]['payment_date'] + '</span></p>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';

      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p>Payment Amount :</p>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p><span>' + paymentsList[i]['payment_amount'] + '</span></p>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';

      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p>Reference No :</p>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p><span>' + paymentsList[i]['reference_no'] + '</span></p>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';



      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<div >';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<div class="next-btn" id="delPayment" onclick="delPayments(' + transaction_id + ',' + payment_id + ',' + payment_amt + ');">';
      daaa1 += '<a>Delete</a>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '<br/>';

    }

    if (paymentsList[i]['payment_type'] == "Cheque") {

      var transaction_id = invoiceid;
      var payment_id = paymentsList[i]['payment_id'];
      var payment_amt = paymentsList[i]['payment_amount'];

      daaa1 += '<div class="purinnertable">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p>Payment Type :</p>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p><span>' + paymentsList[i]['payment_type'] + '</span></p>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';

      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p>Payment Date :</p>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p><span>' + paymentsList[i]['payment_date'] + '</span></p>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';

      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p>Payment Amount :</p>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p><span>' + paymentsList[i]['payment_amount'] + '</span></p>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';

      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p>Account Name :</p>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p><span>' + paymentsList[i]['account_name'] + '</span></p>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';

      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p>Account Head :</p>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p><span>' + paymentsList[i]['account_head'] + '</span></p>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';

      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p>Cheque No :</p>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p><span>' + paymentsList[i]['cheque_no'] + '</span></p>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';

      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p>Bank Name :</p>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p><span>' + paymentsList[i]['bank_name'] + '</span></p>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';

      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p>Branch Name :</p>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p><span>' + paymentsList[i]['branch_name'] + '</span></p>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';

      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p>IFSC :</p>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p><span>' + paymentsList[i]['ifsc'] + '</span></p>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';

      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p>Bank Account No :</p>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p><span>' + paymentsList[i]['bank_account_no'] + '</span></p>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';

      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p>Bank Account Name :</p>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<p><span>' + paymentsList[i]['bank_account_name'] + '</span></p>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';


      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-12">';
      daaa1 += '<div class="row">';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<div >';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '<div class="col-sm-6">';
      daaa1 += '<div class="next-btn" id="delPayment" onclick="delPayments(' + transaction_id + ',' + payment_id + ',' + payment_amt + ');">';
      daaa1 += '<a>Delete</a>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '</div>';
      daaa1 += '<br/>';
    }

  }
  // $(".loading").addClass("spinner");
  $('#divPaymentsList').append(daaa1);
  // $(".loading").removeClass("spinner");

};


//cash book
appdata.cashbookavailablebal = function () {
  var compidObj = {
    company_id: localStorage.getItem('company_id')
  }
  api.post1('getcompanyavailablecash/', compidObj, function (data) {
    var cash_available = (data);
    $('#avb_cash').text(cash_available);
  })
};

appdata.getaccountheadnames = function () {
  $('#acctheadid').empty();
  $('#acctheadid').append('<option value="0"> All</option>');
  api.post1('getaccounthead/', '', function (data) {
    acctheadarry = (data);
    for (var i = 0; i < acctheadarry.length; i++) {
      $('#acctheadid').append('<option value="' + acctheadarry[i].account_head_id + '">' +
        acctheadarry[i].account_head + '</option>');
    }
  })
}

appdata.getcashbooktable = function () {
  var frmdt = $('#fromdate').val();
  var todt = $('#todate').val();
  var fromdateval = new Date(frmdt);
  var todateval = new Date(todt);
  var accountheadid = parseInt($('#acctheadid').val());
  var acct_id = parseInt($('#acctheadname').val());

  var tableobj = {
    company_id: localStorage.getItem('company_id'),
    account_head_id: accountheadid,
    account_id: acct_id,
    from_date: fromdateval,
    to_date: todateval
  }
  var cashbooktable = [];

  api.post1('getcashbook/', tableobj, function (data) {

    cashbooktable = (data.result);
    $(".cashbook-list-attach").removeClass("hide");
    $(".total-balance").removeClass("hide")
    $('#tot_credit').text(data.credit);
    $('#tot_debit').text(data.debit);
    $('#tot_balc').text(data.balance);
    $('#tot_balc_type').text(data.balance_type);

    var txt = '';
    $('.cashbook-list-attach').append(txt);
    for (var i = 0; i < cashbooktable.length; i++) {
      txt += '<div class="cashbook-details">'
      txt += '<div class="cashbook-heading">'
      txt += '<h3 class="book-heading" id="acct_head_val' + i + '">' + cashbooktable[i].account_head + '</h3>'
      txt += '<div class="cashbook-date">'
      txt += '<p>Date :'
      txt += '<span id="cashbook-details-date' + i + '">' + cashbooktable[i].payment_date + '</span>'
      txt += '</p> </div> </div>'

      txt += '<div class="cashbook-amt">'
      txt += '<div class="cash-credit">'
      txt += ' <h5>Credit :</h5>'
      txt += '<p id="cashbook-credit-bal' + i + '">' + cashbooktable[i].credit + '</p>'
      txt += '<div class="clear"></div></div>'
      txt += '<div class="cash-debit">'
      txt += '<h5>debit :</h5>'
      txt += '<p id="cashbook-debit-bal' + i + '">' + cashbooktable[i].debit + '</p>'
      txt += '<div class="clear"></div></div>'
      txt += '<div class="cash-view-details">'
      txt += '<p class="show-details">View Details'
      txt += '<i class="fa fa-chevron-down" aria-hidden="true"></i> </p>'
      txt += '<p class="hide-details">hide Details'
      txt += '<i class="fa fa-chevron-up" aria-hidden="true"></i>'
      txt += '</p> </div> </div>'
      txt += '<div class="cash-dis">'
      txt += '<div class="act-head">'
      txt += '<h5>Account Head :</h5>'
      txt += '<p id="cashbook-acct-heads' + i + '">' + cashbooktable[i].account_head + '</p>'
      txt += '<div class="clear"></div></div>'
      txt += '<div class="act-head">'
      txt += '<h5>Description :</h5>'
      txt += '<p id="cashbook-desc' + i + '">' + cashbooktable[i].description + '</p>'
      txt += '<div class="clear"></div>'
      txt += '</div> </div>'
      txt += '</div>'

    }
    
    $('.cashbook-list-attach').append(txt);
   
    // if (txt != "") {
    //   $('.total-balance').removeClass("hide");
    //   $(".cashbook-list-attach").removeClass("hide");
    //   $("#mytable-body").append(txt);
    // }


  })
}


appdata.getallbankaccounts =function(){
  
  var banklistacc = {
    company_id: localStorage.getItem('company_id'),
  }
  $('#bank-acct-id').empty();
  $('#bank-acct-id').append('<option value="0"> All</option>');

  api.post1('getbankaccountnamesbycompany/', tableobj, function (data){
    console.log(data)
    // $('bank-acct-id').

  })


}



